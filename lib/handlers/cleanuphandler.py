from config import config
from datetime import timedelta


class CleanupHandler(object):
    def __init__(self, conn):
        self.conn = conn

    def process(self):
        with self.conn.cursor() as curs:
            # Clean up deliveries fully complete
            curs.execute("""WITH d AS (
 DELETE FROM outgoing WHERE NOT EXISTS (SELECT 1 FROM outgoing_recipients r WHERE r.outgoing_id=outgoing.id) RETURNING id, queuedts, messageid, sendinglist),
i AS (
 INSERT INTO outgoing_completed (id, messageid, queuedts, completedts, sendinglist) SELECT id, messageid, queuedts, CURRENT_TIMESTAMP, sendinglist FROM d RETURNING id, messageid)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', messageid, 'Completed deliveries of id ' || id || '.' FROM d""")

            # Clean up old whitelist entries
            curs.execute(
                """WITH d AS (
 DELETE FROM lists_listwhitelist WHERE added_at <= now()-%(age)s RETURNING address, list_id)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed whitelist for ' || address || ' from list ' || name || ' (expired).' FROM d LEFT JOIN lists_list ON lists_list.id=d.list_id""",
                {
                    'age': timedelta(days=int(config.get("cleanup", "whitelist_expire_days"))),
                }
            )

            # Clean up old unsubscribe tokens
            curs.execute(
                """WITH d AS (
 DELETE FROM unsubscribe_tokens WHERE tokencreated <= now()-%(age)s RETURNING id)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed ' || count(*) || ' expired unsubscribe tokens.' FROM d HAVING count(*) > 0""",
                {
                    'age': timedelta(days=int(config.get("cleanup", "unsubscribe_expire_days"))),
                }
            )

            # Clean up old cc deduplication records
            curs.execute("""WITH d AS (
 DELETE FROM cc_dedup_messages WHERE added < now()-'60 days'::interval RETURNING added)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed ' || count(*) || ' cc deduplication entries.' FROM d HAVING count(*) > 0""")

            # Clean up old bounces
            curs.execute("""WITH d AS (
 DELETE FROM matched_bounces WHERE dt < now()-'90 days'::interval RETURNING id)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed ' || count(*) || ' matched bounce messages.' FROM d HAVING count(*) > 0""")

            curs.execute("""WITH d AS (
 DELETE FROM matched_notice_bounces WHERE bounce_dt < now()-'90 days'::interval RETURNING id)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed ' || count(*) || ' matched notice bounce messages.' FROM d HAVING count(*) > 0""")

            curs.execute("""WITH d AS (
 DELETE FROM unmatched_bounces WHERE dt < now()-'90 days'::interval RETURNING id)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed ' || count(*) || ' unmatched bounce messages.' FROM d HAVING count(*) > 0""")

            # Clean up old mail history contents (we leave the statistics part around)
            curs.execute("""WITH d AS (
  DELETE FROM mail_history_content WHERE ts < now()-'90 days'::interval RETURNING 1)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed ' || count(*) || ' message history contents.' FROM d HAVING count(*) > 0""")

            # Clean up old log entries
            curs.execute(
                """WITH d AS (
 DELETE FROM log WHERE t < NOW()-%(age)s RETURNING id)
 INSERT INTO log (level, source, messageid, msg) SELECT 0, 'cleanup', NULL, 'Removed ' || count(*) || ' old log entries.' FROM d HAVING count(*) > 0""",
                {
                    'age': timedelta(days=int(config.get("cleanup", "log_expire_days"))),
                }
            )

        self.conn.commit()
