import re
import StringIO
from email.parser import Parser, HeaderParser

from lists import MailingList
from mailutil.header import decode_mime_header
from mailutil.body import get_truncated_body
from mailutil.simple import make_simple_mail
from misc import log
from config import config

archives_bounce_re = re.compile('^(.*)-owner\+archive@(.*)$', re.I)
regular_bounce_re = re.compile('^(.*)-owner\+M(\d+)-(\d+)@(.*)$', re.I)
notice_bounce_re = re.compile('^(.*)-notice\+M(\d+)-([a-z0-9]+)@(.*)$', re.I)

# List of regexps to match against bounce body, to determine if a
# bounce is a transient one rather than a permanent one.
transient_bounces_re = [
    re.compile('spam', re.I),
    re.compile('SPAM', re.I),
    re.compile('Spam', re.I),
    re.compile('Quota', re.I),
    re.compile('quota', re.I),
    re.compile('Administrative prohibition', re.I),
    re.compile('will retry for \d+ more (hours|days)', re.I),
    re.compile('Delivery will continue to be attempted', re.I),
    re.compile('Content reject', re.I),
    re.compile('Message refused', re.I),
    re.compile('Restricted attachment', re.I),
    re.compile('mail server permanently rejected message', re.I),
    re.compile('SigningKeyLookupFailed', re.I),
    re.compile('Message content not allowed', re.I),
    re.compile('Denied by policy', re.I),
    re.compile('Mail content denied\.', re.I),
    re.compile('Message rejected because of unacceptable content\.', re.I),
    re.compile('F-Secure Anti-Virus for Microsoft Exchange has detected undesirable content in your message', re.I),
    re.compile('The message violated one or more content policies', re.I),
    re.compile('Sender ID \(PRA\) Not Permitted', re.I),
    re.compile('Rejected due to Sender-ID policy for sender', re.I),
    re.compile('Mail appears to be unsolicited', re.I),
    re.compile('Mail rejected', re.I),
    re.compile('Local Policy Violation', re.I),
    re.compile('refused by SORBS', re.I),
    re.compile('DNS Blacklisted by safe\.dnsbl\.sorbs\.net', re.I),
    re.compile('Message size exceeds the allowed size for this account', re.I),
    re.compile('Policy reason', re.I),
    re.compile('YOU DO NOT NEED TO RESEND YOUR MESSAGE', re.I),
    re.compile('message too large for this recipient', re.I),
    re.compile('Mail is rejected by recipients\.', re.I),
    re.compile('AOL will not accept delivery of this message\.', re.I),
    re.compile('Mailbox size limit exceeded', re.I),
    re.compile('message was refused by rule', re.I),
    re.compile('Too many connect', re.I),
    re.compile('Content Block', re.I),
    re.compile('Connections not accepted from servers without a valid sender', re.I),
    re.compile('User has full mailbox', re.I),
    re.compile('Mailbox is full', re.I),
    re.compile('Sender is on domain\'s block list', re.I),
    re.compile('Sender denied', re.I),
    re.compile('message content rejected', re.I),
    re.compile('File too large', re.I),
    re.compile('please try later', re.I),
    re.compile('file attachment types not allowed', re.I),
    re.compile('rejecting banned content', re.I),
    re.compile('Message refused by content analysis check', re.I),
    re.compile('Attachment Type Not Permitted - Message Has Been Blocked', re.I),
    re.compile('Message rejected due to local policy.', re.I),
    re.compile('Message denied by policy\.', re.I),
    re.compile('Delivery not authorized, message refused', re.I),
    re.compile('delivery temporarily suspended', re.I),
    re.compile('Rejected by header based manually Blocked Senders', re.I),
    re.compile('DKIM status failed pubkey_unavailable', re.I),
    re.compile('Your message has been rejected due to malicious content', re.I),
    re.compile('BADHEADER', re.I),
    re.compile('empty subject header', re.I),
    re.compile('Empty subject', re.I),
    re.compile('Message could not be delivered for 1 week', re.I),
    re.compile('exceeded storage allocation', re.I),
    re.compile('email delivery standards and policies', re.I),
    re.compile('Your message contained a suspicious attachment', re.I),
    re.compile('prohibited the mail that you sent', re.I),
    re.compile('rejected by system', re.I),
    re.compile('cannot append message to file', re.I),
    re.compile('Rejected by ClamAV', re.I),
    re.compile('Message filtered', re.I),
    re.compile('temporary failure', re.I),
    re.compile('retry timeout exceeded', re.I),
]


class BounceHandler(object):
    def __init__(self, conn, id, recipient, sender, dt, messageid, contents):
        self.conn = conn
        self.curs = conn.cursor()
        self.id = id
        self.recipient = recipient
        self.sender = sender
        self.dt = dt
        self.messageid = messageid
        self.contents = contents
        self.parsed_headers = None

        self.sio = StringIO.StringIO(self.contents)

    def process(self):
        """
        Process an incoming bounce message, matching it up to what kind of bounce it is,
        and deal with possible unsubscription etc based on it.
        """

        # Parse the message header using the python standards for it
        try:
            parser = HeaderParser()
            self.parsed_headers = parser.parse(self.sio)
        except Exception, ex:
            print "Failed to parse message: %s" % ex
            return False

        # We have several patterns of bounces:
        # <list>-owner+M<digits>@<domain> -- bounce for a regular subscriber
        # <list>-owner+archive@<domain>   -- bounce for the archives (should never happen)
        # <list>-notice+M<digits>-<token>@<domain> -- bounce of a held-for-moderation notice
        # So let's figure out what this is
        m = archives_bounce_re.match(self.recipient)
        if m:
            return self.archives_bounce(m)

        m = regular_bounce_re.match(self.recipient)
        if m:
            return self.regular_bounce(m)

        m = notice_bounce_re.match(self.recipient)
        if m:
            # If this is a bounce matching our own bounce sender, then just drop it to avoid
            # circular dropping of moderation entries.
            if notice_bounce_re.match(self.sender):
                log(self.curs, 1, 'bounce', u'Matched bounce from own notification address {0}. Dropping to avoid cycle.'.format(self.sender))
                return True

            return self.notice_bounce(m)

        # Bounce did not match, so store it in the separate table for manual processing
        # XXX: we might want a specific filter here somehow if this gets to be too much,
        # but for now just stick them in that queue.
        return self.unmatched("address did not match pattern")

    def unmatched(self, reason):
        self.curs.execute("INSERT INTO unmatched_bounces (recipient, sender, messageid, dt, contents, reason) SELECT recipient, sender, messageid, dt, contents, %(reason)s FROM bounce_mail WHERE id=%(id)s", {
            'id': self.id,
            'reason': reason,
        })
        log(self.curs, 1, 'bounce', u'Unmatched bounce due to {0}'.format(reason), self.messageid)

        # Flag as processed which will make the caller clean it up from the queue
        return True

    def regular_bounce(self, match):
        """
        Process a regular bounce for a regular subscriber. If the subscriber is still around,
        that is.
        """
        listname = match.group(1)
        subscriberid = int(match.group(2))
        deliveryid = int(match.group(3))
        domainname = match.group(4)

        # First, find if the list exists
        l = MailingList.get_by_address(self.conn, "{0}@{1}".format(listname, domainname))
        if not l:
            return self.unmatched("list {0}@{1} not found".format(listname, domainname))

        # Ok, we found the list, see if we can find the subscriber on this list
        self.curs.execute("SELECT 1 FROM mailinglist_subscribers WHERE listid=%(listid)s AND subscriberaddress_id=%(subscriberid)s", {
            'listid': l.id,
            'subscriberid': subscriberid,
        })
        r = self.curs.fetchall()
        if len(r) == 0:
            return self.unmatched("subscriber {0} is not subscribed to {1}@{2}".format(subscriberid, listname, domainname))
        if len(r) != 1:
            raise Exception("Multiple matches for subscriber {0} on list {1}, should be impossible".format(subscriberid, l.id))

        # This subscriber clearly exists. Record the bounce for further processing.

        headers = "\n".join([u"%s: %s" % (k, decode_mime_header(v)) for k, v in self.parsed_headers.items()])

        transient_re = None

        # Reparse including body, and get a truncated version to store
        try:
            self.sio.seek(0)
            parser = Parser()
            mail = parser.parse(self.sio)
            body = get_truncated_body(mail)
            for r in transient_bounces_re:
                if r.search(body):
                    transient_re = r
                    break

        except Exception:
            body = "Failed to parse message body"

        self.curs.execute("INSERT INTO matched_bounces (list, subscriber, deliveryid, dt, messageid, header, message, transient) VALUES (%(listid)s, %(subscriberid)s, %(deliveryid)s, %(dt)s, %(messageid)s, %(headers)s, %(message)s, %(transient)s)", {
            'listid': l.id,
            'subscriberid': subscriberid,
            'deliveryid': deliveryid,
            'dt': self.dt,
            'messageid': self.messageid,
            'headers': headers,
            'message': body.replace("\0", ""),
            'transient': bool(transient_re),
        })
        if transient_re:
            log(self.curs, 0, 'bounce',
                u'Matched transient bounce to subscriberaddress {0} for delivery {1} (regexp "{2}")'.format(
                    subscriberid,
                    deliveryid,
                    transient_re.pattern,
                ), self.messageid)
        else:
            log(self.curs, 0, 'bounce',
                u'Matched bounce to subscriberaddress {0} for delivery {1}'.format(
                    subscriberid,
                    deliveryid,
                ), self.messageid)

        # The actual unsubscribe-because-of-bounce action will run separtely, so just consider this
        # bounce handled for now.
        return True

    def archives_bounce(self, match):
        # This is a Should Never Happen (TM) thing, so just fire off an email to the list owner as quickly
        # as possible.
        self.curs.execute("INSERT INTO admin_out (sender, recipient, contents) VALUES (%(sender)s, %(recipient)s, %(contents)s)", {
            'sender': config.get('bounces', 'senderaddr'),
            'recipient': config.get('notify', 'address'),
            'contents': make_simple_mail(config.get('bounces', 'senderaddr'),
                                         config.get('bounces', 'sendername'),
                                         config.get('notify', 'address'),
                                         config.get('notify', 'name'),
                                         "Archives bounce",
                                         u"The system received a bounce to {0}.\n\nThis should never happen with archives addresses,\nso the bounce has been flagged as unprocessed for manual check!\n".format(self.recipient),
                                         ).as_string(),
        })
        log(self.curs, 2, 'bounce', 'Matched bounce to archives address!', self.messageid)

        self.curs.execute("NOTIFY admin_out")

        # By returning false we say it's unprocessed, and it will be left in the queue but
        # flagged as failed.
        return False

    def notice_bounce(self, match):
        """
        Process a bouncing notice email. This happens when somebody posts to a list and then
        gets a "held for moderation" notice. When this notice in turn bounces, we get here.
        In this case, find the email still in the moderation queue and drop it from there.
        """
        self.curs.execute("SELECT id FROM mailinglists WHERE domain=%(domain)s AND name=%(name)s", {
            'domain': match.group(4),
            'name': match.group(1),
        })
        if self.curs.rowcount != 1:
            return self.unmatched("Bounced post {0} (token {1}), could not find list {2}@{3}".format(match.group(2), match.group(3), match.group(1), match.group(4)))
        listid, = self.curs.fetchone()

        # Unqueue the mail
        self.curs.execute("DELETE FROM moderation WHERE id=%(id)s AND usertoken=%(token)s AND listid=%(list)s RETURNING id, messageid", {
            'id': match.group(2),
            'token': match.group(3),
            'list': listid,
        })
        rr = self.curs.fetchall()
        if len(rr) == 0:
            return self.unmatched("Bounced post {0} (token {1}) not found in moderation queue".format(match.group(2), match.group(3)))
        if len(rr) != 1:
            raise Exception("Cannot happen -- too many rows matched when removing post!")
        id, messageid = rr[0]

        # Store a copy of the bounce
        self.curs.execute("INSERT INTO matched_notice_bounces (moderationid, list, usertoken, bounce_dt, bounce_msgid, bounce_sender, bounce_contents) VALUES (%(id)s, %(list)s, %(token)s, %(bounce_dt)s, %(bounce_msgid)s, %(bounce_sender)s, %(bounce_contents)s)", {
            'id': id,
            'list': "{0}@{1}".format(match.group(1), match.group(4)),
            'token': match.group(3),
            'bounce_dt': self.dt,
            'bounce_msgid': self.messageid,
            'bounce_sender': self.sender,
            'bounce_contents': self.contents,
        })

        log(self.curs, 0, 'bounce', 'Matched bounce to existing post {0} held for moderation. Post dropped.'.format(messageid), self.messageid)

        return True
