import re
import StringIO
from email.parser import Parser, HeaderParser
from email.utils import parseaddr, getaddresses
from datetime import timedelta
from decimal import Decimal
import hmac
import hashlib
import requests

from lists import MailingList, ModerationReason
from lists import CcPolicies, BccPolicies
from mailutil.header import decode_mime_header
from mailutil.body import get_truncated_body
from misc import generate_random_token, log, format_size
from config import config
from template import send_mailtemplate

re_spamscore = re.compile('^([\d\.-]+) ')
re_unsubscribe = re.compile('^([^@]+)-unsubscribe@([^@]+)$')
re_owner = re.compile('^([^@]+)-owner@([^@]+)$')

# Match the word unsubscribe at the beginning of a line, optionally followed
# by the word me. Only match if within 30 characters of the end of a line.
# Also match some other variants that have proven to be comon
re_body_unsubscribe = [
    re.compile('^(please\s+)?unsubscribe(\s+ m[ey])?.{0,30}$', re.I | re.MULTILINE),
    re.compile('^(please\s+)?remove\s+m[ey].{0,30}$', re.I | re.MULTILINE),
    re.compile('^(please\s+)?remove\s+from.{0,30}$', re.I | re.MULTILINE),
]


class MailHandler(object):
    def __init__(self, conn, id, recipient, sender, messageid, contents):
        self.conn = conn
        self.id = id
        self.recipient = recipient.lower()
        self.sender = sender.lower()
        self.fromaddr = sender.lower()
        self.messageid = messageid
        self.contents = contents
        self.mlist = None
        self.subject = None
        self.to = ''
        self.cc = ''
        self.cclist = []
        self.tagsheader = None
        self.tagssig = None
        self.tags = []
        self.tagnames = []
        self.spamscore = None

        self.sio = StringIO.StringIO(self.contents)
        self._truncated_body = None

    def process_from_moderation(self, listid, subject):
        # We are still going to need the headers, to parse those
        mail = self.parse_headers()
        if not mail:
            # Should never happen since it worked once, but just in case
            return False

        self.mlist = MailingList.get_by_id(self.conn, listid)
        if not self.mlist:
            raise Exception("Mailing list not found, should never happen")

        self.subject = subject

        if self.mlist.tagged_delivery:
            self.parse_and_verify_tags()
            # Since the email was approved by moderators, it means it should be delivered even
            # if there are errors in the tags. In this case, it will get delivered only to
            # those subscribers who asked to receive *all* email, regardless of tags.

        # Don't check for moderation etc, as this mail has already been approved
        self.deliver(True)

        # Delivery succeeded, so send a notice to the original user
        with self.conn.cursor() as curs:
            if self.sender != '':
                # Don't send "released" notices to the sender if it's empty, because that will just get
                # stuck.
                send_mailtemplate(curs,
                                  self.mlist.owner_address(),
                                  self.mlist.owner_name(),
                                  self.sender,
                                  '',  # Sender name is unknown
                                  u"Message to {0} released from moderation".format(self.mlist.name),
                                  'moderation_released.txt', {
                                      'subject': self.subject,
                                      'webroot': config.get("web", "root"),
                                      'name': self.mlist.name,
                                  })
        return True

    def parse_headers(self):
        # Parse the message using the python standards for it
        try:
            parser = HeaderParser()
            mail = parser.parse(self.sio)
        except Exception, ex:
            # Start by printing in case things went *really* bad, so this goes
            # in the daemon log.
            print "Failed to parse message: %s" % ex

            # Then log to db as well.
            with self.conn.cursor() as curs:
                log(curs, 2, 'mail', u"Failed to parse message: %s" % ex)
            return False

        try:
            self.subject = decode_mime_header(mail['Subject'])
        except:
            self.subject = "Unknown subject"

        try:
            self.fromaddr = parseaddr(decode_mime_header(mail['From']))[1].lower()
        except:
            # Fallback to envelope sender if unparseable
            self.fromaddr = self.sender.lower()

        # Collect recipient addresses from To and Cc - used for eliminatecc
        hl = []
        if 'To' in mail:
            self.to = decode_mime_header(mail['To'])
            hl.append(self.to)
        if 'Cc' in mail:
            self.cc = decode_mime_header(mail['Cc'])
            hl.append(self.cc)

        self.cclist = [a[1].lower() for a in getaddresses(hl)]

        # Collect references messageids - used for blacklisting
        hl = []
        if 'In-Reply-To' in mail:
            hl.extend(decode_mime_header(mail['In-Reply-To']).split())
        if 'References' in mail:
            hl.extend(decode_mime_header(mail['References']).split())
        self.referenceslist = [x.lstrip('<').rstrip('>') for x in hl]

        # Copy the tags header and key if they are present. We don't validate them
        # here because we don't know which list they are on yet, so we can't find
        # the key.
        if 'X-pglister-tags' in mail:
            self.tagsheader = decode_mime_header(mail['X-pglister-tags'])
        if 'X-pglister-tagsig' in mail:
            self.tagssig = decode_mime_header(mail['X-pglister-tagsig'])

        if 'X-Pg-Spam-Score' in mail:
            # Get the actual value
            try:
                m = re_spamscore.match(mail['X-Pg-Spam-Score'])
                if m:
                    self.spamscore = Decimal(m.groups(1)[0])
                else:
                    with self.conn.cursor() as curs:
                        log(curs, 1, 'mail', 'Spamscore header found, but in invalid format. Ignoring.', self.messageid)
            except Exception, ex:
                with self.conn.cursor() as curs:
                    log(curs, 1, 'mail', 'Spamscore extraction failed: %s' % ex, self.messageid)

        return mail

    def parse_and_verify_tags(self):
        if not self.tagsheader or not self.tagssig:
            return False

        h = hmac.new(self.tagsheader.encode('ascii', 'ignore'), self.mlist.tagkey, hashlib.sha256)
        hd = h.hexdigest()
        if not hmac.compare_digest(hd, self.tagssig.encode('ascii', 'ignore')):
            return "HMAC of tags header does not match"

        if not re.match('^[a-z0-9,]+$', self.tagsheader):
            return "Tags header contains invalid characters"

        tagnames = self.tagsheader.split(',')

        # Figure out if all tags are valid
        with self.conn.cursor() as curs:
            tried_refresh = False
            while True:
                curs.execute("SELECT id FROM lists_listtag WHERE list_id=%(listid)s AND tag=ANY(%(tags)s)", {
                    'tags': tagnames,
                    'listid': self.mlist.id,
                })
                if curs.rowcount == len(tagnames):
                    # All tags found!
                    self.tags = [tid for tid, in curs.fetchall()]
                    self.tagnames = tagnames
                    break

                # Mail references a tag that doesn't exist. Make an attempt to refresh the
                # tags. But only one
                if not tried_refresh:
                    self.refresh_tags()
                    tried_refresh = True
                else:
                    # Still no good. Ok, figure out which tags are missing for the error msg
                    curs.execute("SELECT n FROM UNNEST(%(tags)s) n(n) WHERE NOT EXISTS (SELECT 1 FROM lists_listtag WHERE list_id=%(listid)s AND tag=n)", {
                        'tags': tagnames,
                        'listid': self.mlist.id,
                    })
                    return "Unknown tag(s) specified: {0}".format(",".join([t for t, in curs.fetchall()]))

    def refresh_tags(self):
        # Refresh the tags for this specific list.
        # API call is expected to use https, so we trust that, no need for a separate
        # validation.
        # Note! Does not update descriptions of tags, just adds/removes existing ones.
        with self.conn.cursor() as curs:
            try:
                r = requests.get(self.mlist.taglistsource)
                if r.status_code != 200:
                    log(curs, 2, 'mail', u"Failed to load taglist for {0}: status code {1}".format(self.mlist.name, r.statuscode))
                    return
                taglist = r.json()
                curs.execute("INSERT INTO lists_listtag(list_id, tag, description) SELECT %(listid)s, (%(tags)s)[g], (%(tagdesc)s)[g] FROM generate_series(1,%(len)s) g(g) WHERE NOT EXISTS (SELECT 1 FROM lists_listtag l2 WHERE l2.list_id=%(listid)s AND l2.tag=(%(tags)s)[g])", {
                    'listid': self.mlist.id,
                    'tags': [t['name'] for t in taglist['tags']],
                    'tagdesc': [t['description'] for t in taglist['tags']],
                    'len': len(taglist['tags']),
                })
                if curs.rowcount > 0:
                    log(curs, 0, 'mail', u"Loaded new tags {0} for list {1}".format(
                        ",".join([t for t, in curs.fetchall()]), self.mlist.name))
                curs.execute("DELETE FROM lists_listtag WHERE list_id=%(listid)s AND NOT tag=ANY(%(tags)s) RETURNING tag", {
                    'listid': self.mlist.id,
                    'tags': [t['name'] for t in taglist['tags']],
                })
                if curs.rowcount > 0:
                    log(curs, 0, 'mail', u"Removed tags {0} from list {1}".format(
                        ",".join([t for t, in curs.fetchall()]), self.mlist.name))
            except Exception, e:
                log(curs, 2, 'mail', u"Failed to load taglist for {0}: Exception {1}".format(self.mlist.name, e))
                return

    def process(self):
        # First, make a basic deduplication check. If the same
        # messageid has already been delivered *to the same list*, we
        # don't deliver it again. Same messageid to two different
        # lists need to be processed since they have different
        # subscribers, but to the same list is a pure duplicate that
        # can be caused by a network issue or more importantly in the
        # case where we have the same list available through multiple
        # domains and the user actually CCed both lists.
        with self.conn.cursor() as curs:
            curs.execute("SELECT EXISTS (SELECT 1 FROM outgoing WHERE messageid=%(msgid)s AND sendinglist=%(listaddr)s) OR EXISTS (SELECT 1 FROM outgoing_completed WHERE messageid=%(msgid)s AND sendinglist=%(listaddr)s)", {
                'msgid': self.messageid,
                'listaddr': self.recipient,
            })
            if curs.fetchone()[0]:
                # Duplicate message, so just drop it.
                log(curs, 1, 'mail', 'Message already delivered to {0}, ignoring this copy'.format(self.recipient), self.messageid)
                return True

        # Parse the actual mail
        mail = self.parse_headers()
        if not mail:
            return False

        # Start by checking global blacklist and just drop those
        # emails on the floor. We check the blacklist against both
        # envelope sender and from address.
        with self.conn.cursor() as curs:
            curs.execute("SELECT email FROM mailinglist_globalblacklist WHERE email=%(sender)s OR email=%(from)s OR (email LIKE '/%%' AND (%(sender)s ~ substring(email,2) OR %(from)s ~ substring(email,2))) LIMIT 1", {
                'sender': self.sender,
                'from': self.fromaddr,
            })
            if curs.rowcount > 0:
                # Present in blacklist! Floor, here we come!
                pattern = curs.fetchone()[0]
                curs.execute("UPDATE mailinglist_globalblacklist SET usecount=usecount+1, lastused=CURRENT_TIMESTAMP WHERE email=%(email)s", {
                    'email': pattern,
                })
                log(
                    curs, 1, 'mail',
                    u"Sender {0} ({1}) found on global blacklist ({2}), dropping mail.".format(self.sender, self.fromaddr, pattern),
                    self.messageid,
                )
                self.store_history(4)
                return True

        # Handle possible unsubscribe request
        m = re_unsubscribe.match(self.recipient)
        if m:
            # Format is <something>-unsubscribe@<something>. So clearly an unsubscribe
            # request. Find the mailinglist <something>@<something>.
            self.mlist = MailingList.get_by_address(self.conn, u"{0}@{1}".format(*m.groups()))
            if not self.mlist:
                # This really should never happen, since exim should've
                # validated the list already.
                with self.conn.cursor() as curs:
                    log(curs, 2, 'mail',
                        u"Could not find list '{0}@{1}' for unsubscribe".format(*m.groups()),
                        self.messageid,
                        )
                # For now, we leave this mail in the incoming queue to process manually, since
                # it might indicate a bug.
                return False

            self.process_unsubscribe()
            return True

        m = re_owner.match(self.recipient)
        if m:
            self.mlist = MailingList.get_by_address(self.conn, u"{0}@{1}".format(*m.groups()))
            if not self.mlist:
                # This really should never happen, since exim should've
                # validated the list already.
                with self.conn.cursor() as curs:
                    log(curs, 2, 'mail',
                        u"Could not find list '{0}@{1}' for owner expansion".format(*m.groups()),
                        self.messageid,
                        )
                # For now, we leave this mail in the incoming queue to process manually, since
                # it might indicate a bug.
                return False

            self.process_owner()
            return True

        # We have a parsable message. Find the destination for it.
        self.mlist = MailingList.get_by_address(self.conn, self.recipient)
        if not self.mlist:
            # This really should never happen, since exim should've
            # validated the list already.
            with self.conn.cursor() as curs:
                log(
                    curs, 2, 'mail',
                    u"Could not find list {0} for post".format(self.recipient),
                    self.messageid,
                )
            # For now, we leave this mail in the incoming queue to
            # process manually, since it might indicate a bug.
            return False

        # Check if this address is personally blacklisted.
        with self.conn.cursor() as curs:
            curs.execute("SELECT email FROM mailinglist_personalblacklist WHERE email=%(sender)s OR email=%(from)s", {
                'sender': self.sender,
                'from': self.fromaddr,
            })
            if curs.rowcount:
                # User is sending from a blacklisted address, so reply
                # with a message about that.
                send_mailtemplate(curs,
                                  self.mlist.owner_address(),
                                  self.mlist.owner_name(),
                                  self.sender,
                                  '',
                                  u'Email sent to {0}'.format(self.mlist.name),
                                  'blacklist_response.txt', {
                                      'list': self.mlist.name,
                                      'email': curs.fetchone()[0],
                                      'webroot': config.get("web", "root"),
                                  })
                log(curs, 1, 'mail',
                    u"Sender {0} ({1}) is on personal blacklist, email dropped and sender notified".format(self.sender, self.fromaddr),
                    self.messageid)
                return True

            # Check if a blacklisted address is in CC or TO
            # (because we want to send a different email in this case)
            curs.execute("SELECT email FROM mailinglist_personalblacklist WHERE email=ANY(%(cclist)s)", {
                'cclist': self.cclist,
            })
            if curs.rowcount:
                matched_mail = curs.fetchone()[0]
                log(curs, 1, 'mail',
                    u"Recipient {0} is on personal blacklist but CCed on email, email dropped and sender notified".format(matched_mail),
                    self.messageid)
                send_mailtemplate(curs,
                                  self.mlist.owner_address(),
                                  self.mlist.owner_name(),
                                  matched_mail,
                                  '',
                                  u'Email sent to {0}'.format(self.mlist.name),
                                  'blacklist_notify.txt', {
                                      'list': self.mlist.name,
                                      'email': matched_mail,
                                      'sender': self.sender,
                                      'webroot': config.get("web", "root"),
                                  })
                send_mailtemplate(curs,
                                  self.mlist.owner_address(),
                                  self.mlist.owner_name(),
                                  self.sender,
                                  '',
                                  u'Email sent to {0}'.format(self.mlist.name),
                                  'blacklist_other.txt', {
                                      'list': self.mlist.name,
                                      'email': matched_mail,
                                      'webroot': config.get("web", "root"),
                                  })
                return True

        # Check against CC and BCC policies
        if self.mlist.bcc_policy > 0:
            # Check that this list, or one of it's aliases, is either in the To
            # or the Cc field.
            with self.conn.cursor() as curs:
                curs.execute("SELECT alias FROM lists_listalias WHERE list_id=%(listid)s", {
                    'listid': self.mlist.id,
                })
                aliases = [r for r, in curs.fetchall()]
                aliases.append(self.mlist.address)

                if not set(self.cclist).intersection(aliases):
                    # Intesection is empty, means that nothing from aliases was in cclist
                    if self.mlist.bcc_policy == BccPolicies.MODERATE:
                        self.moderate(ModerationReason(ModerationReason.BCC))
                    else:
                        # With no list in CC or BCC, drop is done without notification
                        # because it's almost certainly spam.
                        log(curs, 1, 'mail',
                            u"Message had no list address or alias in To or Cc, silently discarded.",
                            self.messageid)
                        # Store in history if it didn't get moderated. If it was moderated, it will
                        # be stored in history when moderation is completed.
                        self.store_history(5)
                    return True

        if self.mlist.cc_policy > 0:
            # Count the number of lists tihs email is directed to that do not allow
            # CCs. This means that if an email is CCed between a list not allowing CC
            # and one allowing CC, it will be allowed, but if two or more lists that
            # don't allow CC are there, then not.
            with self.conn.cursor() as curs:
                curs.execute(
                    """
SELECT m.address FROM mailinglists m
WHERE m.cc_policy > 0
AND (
 address=ANY(%(addr)s)
 OR EXISTS (SELECT 1 FROM lists_listalias la WHERE la.list_id=m.id AND la.alias=ANY(%(addr)s))
)""",
                    {
                        'addr': self.cclist,
                    }
                )
                if curs.rowcount > 1:
                    lists = [r for r, in curs.fetchall()]
                    # Make sure we don't duplicate the sending
                    curs.execute("INSERT INTO cc_dedup_messages (messageid) VALUES (%(messageid)s) ON CONFLICT DO NOTHING", {
                        'messageid': self.messageid,
                    })
                    if curs.rowcount > 0:
                        # If our insert was successful, this was the first time we saw this messageid in
                        # the cc scenario. So send notification to the user.
                        send_mailtemplate(curs,
                                          self.mlist.owner_address(),
                                          self.mlist.owner_name(),
                                          self.sender,
                                          '',  # Sender name is unknown
                                          u'Message to multiple lists',
                                          'cc_notification.txt', {
                                              'lists': "\n".join(["* {}".format(l) for l in lists]),
                                          })

                        log(curs, 1, 'mail',
                            u"Message was CCed to {0}: dropped and notification sent".format(", ".join(lists)),
                            self.messageid)
                    else:
                        log(curs, 1, 'mail',
                            u"Message was CCed to {0}: dropped (notification previously sent)".format(", ".join(lists)),
                            self.messageid)
                    self.store_history(6)
                    return True

        # So we have the list that we should send this email to.
        # Start by figuring out if it needs to be moderated.
        reason = self.mlist.moderation_reason(self.fromaddr, self.messageid)
        if reason:
            # Yup it does
            self.moderate(reason)
            return True

        if self.mlist.tagged_delivery:
            r = self.parse_and_verify_tags()
            if r:
                self.moderate(ModerationReason(ModerationReason.TAGERROR, "Failed to parse tags: %s" % r))
                return True

            # If we successfully parsed but there are no tags, send to moderation
            if not self.tags:
                self.moderate(ModerationReason(ModerationReason.TAGERROR, "No tags specified, this list requires tags"))
                return True

            # Else we have valid tags. So fall through to possible delivery

        # Maybe the message is too big?
        if self.sio.len > self.mlist.maxsizedrop:
            # Email is so big it should be dropped!
            with self.conn.cursor() as curs:
                log(curs, 1, 'mail',
                    u"Message {0} size {1} ({2} bytes) is larger than drop threshold {3} ({4} bytes), message dropped.".format(
                        self.messageid,
                        format_size(self.sio.len),
                        self.sio.len,
                        format_size(self.mlist.maxsizedrop),
                        self.mlist.maxsizedrop,
                    ), self.messageid)
            # Message deleted when we say we've handled it
            self.store_history(7)
            return True
        elif self.sio.len > self.mlist.maxsize:
            self.moderate(ModerationReason(
                ModerationReason.SIZE,
                "Size {0} ({1} bytes) is larger than threshold {2} ({3} bytes)".format(
                    format_size(self.sio.len),
                    self.sio.len,
                    format_size(self.mlist.maxsize),
                    self.mlist.maxsize
                )
            ))
            return True

        # It might also be spam-tagged
        if self.spamscore and self.spamscore > self.mlist.spamscore_threshold:
            self.moderate(ModerationReason(ModerationReason.SPAM, "Spamscore {0} is higher than threshold {1}".format(self.spamscore, self.mlist.spamscore_threshold)))
            return True

        # Does it look like a direct request to unsubscribe? We send those to moderation to avoid the risk of
        # spamming the list membership with them. At least this way we only spam the moderators.
        lsubj = self.subject.lower()
        for k in ('unsubscribe', 'unsibscribe', 'unscribe', 'remove me', 'remove my'):
            if k in lsubj:
                self.moderate(ModerationReason(ModerationReason.UNSUBSCRIBE, "subject"))
                return True

        for bu in re_body_unsubscribe:
            if bu.search(self._get_truncated_body()):
                self.moderate(ModerationReason(ModerationReason.UNSUBSCRIBE, "body"))
                return True

        # Does it appear on our hold-these-messageids list?
        with self.conn.cursor() as curs:
            curs.execute("SELECT messageid FROM lists_messageidmoderationlist WHERE messageid=ANY(%(idlist)s) LIMIT 1", {
                'idlist': self.referenceslist,
            })
            if curs.rowcount > 0:
                matched, = curs.fetchone()
                self.moderate(ModerationReason(ModerationReason.MODERATELIST, matched))
                return True

        # It might also be matching keyword regexps if there are any
        # (we make this check last as it's the most expensive one)
        for r in self.mlist.moderation_regexes:
            if r.search(self._get_truncated_body()):
                self.moderate(ModerationReason(ModerationReason.MODERATEREGEX, r.pattern))
                return True

        # Check global moderaiton lists
        with self.conn.cursor() as curs:
            curs.execute("SELECT expression, moderate_sender, moderate_subject, moderate_body FROM mailinglist_globalmodlist WHERE moderate_sender OR moderate_subject OR moderate_body")
            for expression, m_send, m_subj, m_body in curs.fetchall():
                r = re.compile(expression, re.I)
                if m_send:
                    if r.search(self.sender) or r.search(self.fromaddr):
                        self.moderate(ModerationReason(ModerationReason.GLOBALMODERATEREGEX, "sender: {0}".format(r.pattern)))
                        return True
                if m_subj:
                    if r.search(self.subject):
                        self.moderate(ModerationReason(ModerationReason.GLOBALMODERATEREGEX, "subject: {0}".format(r.pattern)))
                        return True
                if m_body:
                    if r.search(self._get_truncated_body()):
                        self.moderate(ModerationReason(ModerationReason.GLOBALMODERATEREGEX, "body: {0}".format(r.pattern)))
                        return True

        # Sender was correct, list was not in full moderate (or any
        # other flags controlled by the list), and no spamscore. That
        # means we are ready to deliver.

        self.deliver(False)
        return True

    def _get_truncated_body(self):
        # Get the truncated body text of the message, caching the
        # result in case we need to use it more than once.
        if not self._truncated_body:
            try:
                self.sio.seek(0)
                parser = Parser()
                mail = parser.parse(self.sio)
                self._truncated_body = get_truncated_body(mail)
                # psypcopg2 and postgres don't like strings with NUL in it
                if "\0" in self._truncated_body:
                    self._truncated_body = self._truncated_body("\0", " ")
            except Exception, ex:
                self._truncated_body = u"Unable to determine body text:\n{0}".format(ex)
        return self._truncated_body

    def moderate(self, reason):
        with self.conn.cursor() as curs:
            # Stick the email in the moderation queue by copying it from
            # the incoming queue.
            usertoken = generate_random_token()

            curs.execute("INSERT INTO moderation (origid, listid, sender, fromaddr, recipient, messageid, contents, usertoken, moderatedat, reason, reasonextra, subject, intro, _to, cc, spamscore) SELECT id, %(listid)s, sender, %(fromaddr)s, recipient, messageid, contents, %(usertoken)s, now(), %(reason)s, %(reasonextra)s, %(subject)s, %(intro)s, %(to)s, %(cc)s, %(spamscore)s FROM incoming_mail WHERE id=%(id)s RETURNING id, pg_size_pretty(length(contents)::bigint) as size", {
                'id': self.id,
                'listid': self.mlist.id,
                'fromaddr': self.fromaddr,
                'usertoken': usertoken,
                'reason': reason.reason,
                'reasonextra': reason.extra,
                'subject': self.subject,
                'intro': self._get_truncated_body(),
                'to': self.to,
                'cc': self.cc,
                'spamscore': self.spamscore,
            })
            modid, size, = curs.fetchone()

            modinfo = {
                'from': self.fromaddr,
                'sender': self.sender,
                'size': size,
                'name': self.mlist.name,
                'subject': self.subject,
                'webroot': config.get("web", "root"),
                'usertoken': usertoken,
                'truncbody': self._get_truncated_body(),
            }

            if self.mlist.send_moderation_notices and self.sender != '':
                # Don't send moderation notices to the sender if it's empty, because that will just get
                # stuck. The notices to moderators still goes out, so they can decide what to do.
                send_mailtemplate(
                    curs,
                    "{0}-notice+M{1}-{2}@{3}".format(
                        self.mlist.name,
                        modid,
                        usertoken,
                        self.mlist.domain,
                    ),
                    self.mlist.owner_name(),
                    self.sender,
                    '',  # Sender name is unknown
                    u"Message to {0} held for moderation".format(self.mlist.name),
                    'moderation_response.txt',
                    modinfo,
                )

            # Queue moderation notices for all moderators of this list.
            # Notifications are queued with a delay to give the poster
            # a chance to cancel the message. For multiple moderators,
            # spread out the notifications so that if the first moderator
            # picks it up, others don't have to see it. In this case,
            # randomize the order between moderators.
            curs.execute("WITH t AS (SELECT id FROM lists_list_moderators WHERE list_id=%(listid)s ORDER BY random()) INSERT INTO moderator_notices (moderation_id, moderator_id, sendat, token) SELECT %(modid)s, id, now()+row_number() over() * %(ival)s, encode(pgcrypto.digest(pgcrypto.gen_random_bytes(250),'sha256'),'hex') FROM t RETURNING id", {
                'listid': self.mlist.id,
                'modid': modid,
                'ival': timedelta(minutes=config.getint('moderation', 'notice_delay')),
            })
            moderators = curs.fetchall()

            log(curs, 0, 'mail',
                u"Message to list {0} held for moderation due to '{1}', notice queued for {2} moderators".format(self.mlist.name, reason.full_string(), len(moderators)),
                self.messageid)

    def deliver(self, from_moderation=True):
        # Try to build a new message while touching as little as possible
        # of the actual contents of the current message. That means we can't
        # send it through the python parsing routines as they will change
        # things. But all we need to detect here is when the message
        # switches from headers to body, and insert our headers before
        # that.

        # The StringIO has already been used by the parser, so send it
        # back to the beginning.
        self.sio.seek(0)

        targethdr = StringIO.StringIO()
        targetbody = StringIO.StringIO()

        # Look for end of headers, accumulating all headers until we
        # reach it.
        while True:
            l = self.sio.readline()
            if l.rstrip("\r\n") == '':
                # Empty line means we've hit the header boundary. Write
                # our own custom headers and then switch to body mode.
                self.mlist.writeheaders(targethdr)
                break
            else:
                targethdr.write(l)

        # Now copy over the body.
        while True:
            t = self.sio.read(8192)
            if t:
                targetbody.write(t)
            else:
                break

        # Let's write this!
        targethdr.seek(0)
        targetbody.seek(0)

        with self.conn.cursor() as curs:
            curs.execute("INSERT INTO outgoing (sendinglist, messageid, headers, body) VALUES (%(sender)s, %(messageid)s, %(header)s, %(body)s) RETURNING id", {
                'sender': self.mlist.address,
                'messageid': self.messageid,
                'header': bytearray(targethdr.getvalue()),
                'body': bytearray(targetbody.getvalue()),
            })
            id = curs.fetchone()[0]

            # Write our entries in "order of domain name", so that we can
            # give exim a better chance of delivering quickly. We do the
            # ordering here instead of in the posting process so that we
            # only have to do the more expensive ORDER BY once, and we can
            # do an indexed order by in the frequent process.
            params = {
                'outid': id,
                'listid': self.mlist.id,
                'cclist': self.cclist,
                'webroot': config.get("web", "root"),
            }
            if not self.mlist.tagged_delivery:
                # Normal delivery
                qextra = ""
            else:
                # Deliver either if subscribed to this tag, or subscribed to no tags at all
                qextra = " AND (tags IS NULL OR tags && %(tagids)s)"
                params['tagids'] = self.tags
                log(curs, 0, 'mail',
                    u'Mail to list {0} delivered to {1} tags ({2})'.format(self.mlist.name, len(self.tags), u",".join(self.tagnames)),
                    self.messageid)

            curs.execute("INSERT INTO outgoing_recipients (outgoing_id, subscriberaddress_id,recipient_headers) SELECT %(outid)s, subscriberaddress_id, recipient_list_headers(%(webroot)s, %(listid)s, token) FROM mailinglist_subscribers s WHERE listid=%(listid)s {0} AND NOT (COALESCE(eliminatecc, false) AND email=ANY(%(cclist)s)) ORDER BY split_part(email, '@', 2)".format(qextra), params)
            recipientcount = curs.rowcount
            curs.execute("NOTIFY outgoing")

            log(curs, 0, 'mail',
                u'Mail to list {0} queued for delivery to {1} recipients.'.format(self.mlist.name, recipientcount),
                self.messageid)

            # If archiving, add that as well. We send those messages out
            # using the admin queue,since we don't have as advanced VERP
            # processing here.
            if self.mlist.archive_server:
                curs.execute("INSERT INTO raw_out (sender, recipient, contents) VALUES (%(sender)s, %(recipient)s, %(header)s || chr(13)::bytea || chr(10)::bytea || %(body)s)", {
                    'sender': self.mlist.owner_address().replace('@', '+archive@'),
                    'recipient': self.mlist.archive_submit_address(),
                    'header': bytearray(targethdr.getvalue()),
                    'body': bytearray(targetbody.getvalue()),
                })
                log(curs, 0, 'mail',
                    u'Mail to list {0} queued for delivery to archives.'.format(self.mlist.name),
                    self.messageid)
                curs.execute("NOTIFY raw_out")

        self.store_history(from_moderation and 1 or 0)

    def store_history(self, status):
        with self.conn.cursor() as curs:
            curs.execute("INSERT INTO mail_history_content (messageid, contents) values (%(messageid)s, %(contents)s)", {
                'messageid': self.messageid,
                'contents': bytearray(self.sio.getvalue()),
            })

            # Permanent storage mainly for statistics
            curs.execute("INSERT INTO mail_history(messageid, spamscore, status) VALUES (%(messageid)s, %(spamscore)s, %(status)s)", {
                'messageid': self.messageid,
                'spamscore': self.spamscore,
                'status': status,
            })

    def process_owner(self):
        # Process an email sent to the owner of a list. In this case, we
        # expand the recipients to be all the current moderators, and
        # pass the mail on to there.
        with self.conn.cursor() as curs:
            mods = self.mlist.get_moderators()
            for email, name in mods:
                curs.execute("INSERT INTO raw_out (sender, recipient, contents) VALUES (%(sender)s, %(recipient)s, %(contents)s)", {
                    'sender': self.sender,
                    'recipient': email,
                    'contents': bytearray(self.sio.getvalue()),
                })
            log(curs, 0, 'mail',
                u'Mail to list owner of {0} delivered to {1} moderators.'.format(self.mlist.name, len(mods)),
                self.messageid)
            curs.execute("NOTIFY raw_out")

    def process_unsubscribe(self):
        # Find out if the user is subscribed in the first place
        with self.conn.cursor() as curs:
            curs.execute("SELECT subscriberaddress_id FROM mailinglist_subscribers WHERE listid=%(id)s AND email=%(email)s", {
                'id': self.mlist.id,
                'email': self.fromaddr,
            })
            r = curs.fetchone()
            if not r:
                log(curs, 1, "mail",
                    u"Address {0} is not subscribed to list {1}.".format(self.fromaddr, self.mlist.name),
                    self.messageid)
                return

            # Generate unsubscribe token
            token = generate_random_token()
            curs.execute("INSERT INTO unsubscribe_tokens (listid, subscriberaddress, token) VALUES (%(listid)s, %(subscriberaddress)s, %(token)s)", {
                'listid': self.mlist.id,
                'subscriberaddress': r[0],
                'token': token,
            })

            # Generate email asking to click on said token
            send_mailtemplate(curs,
                              self.mlist.owner_address(),
                              self.mlist.owner_name(),
                              self.fromaddr,
                              '',
                              u"Unsubscribe from {0}".format(self.mlist.name),
                              'unsubscribe_request.txt', {
                                  'name': self.mlist.name,
                                  'webroot': config.get("web", "root"),
                                  'token': token,
                              })

            log(curs, 0, 'mail',
                u'Generate unsubscribe token for {0} from list {1}'.format(self.fromaddr, self.mlist.address),
                self.messageid)
