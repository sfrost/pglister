from config import config
from lists import MailingList, ModerationReason
from template import send_mailtemplate


class ModerationNoticeSender(object):
    def __init__(self, conn):
        self.conn = conn

    def process_next(self):
        with self.conn.cursor() as curs:
            curs.execute("""
SELECT
    moderator_notices.id,
    moderation_id,
    moderator_id,
    list_id,
    u.first_name || ' ' || u.last_name AS name,
    u.email,
    fromaddr,
    sender,
    pg_size_pretty(length(contents)::bigint) AS size,
    subject,
    reason,
    reasonextra,
    token,
    usertoken,
    intro
FROM
    moderator_notices
    INNER JOIN moderation ON moderation.id=moderator_notices.moderation_id
    INNER JOIN lists_list_moderators llm ON llm.id=moderator_id INNER JOIN lists_subscriber ls ON ls.user_id=llm.subscriber_id
    INNER JOIN auth_user u ON u.id=ls.user_id
WHERE
    NOT sent
    AND sendat<=CURRENT_TIMESTAMP
LIMIT 1
FOR UPDATE""")
            ll = curs.fetchall()
            if len(ll) == 0:
                # Nothing left, so we're done
                self.conn.rollback()
                return False

            (id, moderation_id, moderator_id, list_id, name, email, fromaddr, sender, size, subject, reason, reasonextra, token, usertoken, intro) = ll[0]
            mlist = MailingList.get_by_id(self.conn, list_id)
            modinfo = {
                'from': fromaddr.decode('utf8'),
                'sender': sender.decode('utf8'),
                'size': size,
                'name': mlist.name.decode('utf8'),
                'subject': subject.decode('utf8'),
                'reason': ModerationReason(reason, reasonextra).full_string(),
                'webroot': config.get("web", "root"),
                'token': token,
                'usertoken': usertoken,
                'truncbody': intro.decode('utf8'),
            }

            send_mailtemplate(curs,
                              mlist.moderator_notice_address(),
                              mlist.moderator_notice_name(),
                              email,
                              name,
                              u"Moderation notice for {0}".format(mlist.name),
                              'moderation_notice.txt',
                              modinfo)

            curs.execute("UPDATE moderator_notices SET sent = true WHERE id=%(id)s", {'id': id})
            self.conn.commit()
        return True
