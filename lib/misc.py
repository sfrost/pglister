from Crypto.Hash import SHA256
from Crypto import Random


def generate_random_token():
    s = SHA256.new()
    r = Random.new()
    s.update(r.read(250))
    return s.hexdigest()


def log(curs, level, source, msg, messageid=None):
    if level < 0:
        level = 0
    if level > 2:
        level = 2

    curs.execute("INSERT INTO log (level, source, messageid, msg) VALUES (%(level)s, %(source)s, %(msgid)s, %(msg)s)", {
        'level': level,
        'source': source,
        'msgid': messageid,
        'msg': msg,
    })


def format_size(bytes):
    try:
        bytes = int(bytes)
    except (TypeError, ValueError, UnicodeDecodeError):
        raise ValueError

    if bytes < (1 << 10):
        return str(bytes)
    elif bytes < (1 << 20):
        return "{0}KB".format(bytes / (1 << 10))
    elif bytes < (1 << 30):
        return "{0}MB".format(bytes / (1 << 20))
    else:
        return "{0}GB".format(bytes / (1 << 30))
