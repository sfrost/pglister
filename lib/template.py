import codecs
import os.path
import string

from mailutil.simple import make_simple_mail

from misc import log


def render_mailtemplate(templatename, args=None, **kwargs):
    with codecs.open(os.path.abspath(os.path.join(os.path.dirname(__file__), '../mailtemplates', templatename)), encoding='utf-8') as f:
        t = string.Template(f.read())
        return t.substitute(args, **kwargs)


def send_mailtemplate(curs, sender, sendername, recipient, recipientname, subject, templatename, args=None):
    if recipient == '':
        # Don't allow sending of emails to empty senders, instead log an error
        log(curs, 2, 'internal', "Refusing to send template based mail from '{0}' to '{1}', subject '{2}'".format(
            sender,
            recipient,
            subject,
        ))
        return

    curs.execute("INSERT INTO admin_out (sender, recipient, contents) VALUES (%(sender)s, %(recipient)s, %(content)s)", {
        'sender': sender,
        'recipient': recipient,
        'content': make_simple_mail(
            sender,
            sendername,
            recipient,
            recipientname,
            subject,
            render_mailtemplate(templatename, args),
        ).as_string(),
    })

    curs.execute("NOTIFY admin_out")
