import os
import sys
import ConfigParser

import psycopg2


class Config(object):
    def __init__(self):
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read(os.path.abspath(os.path.join(os.path.dirname(__file__), '../pglister.ini')))
        self._conn = None

        try:
            self.connstr = self.cfg.get('db', 'connstr')
        except:
            print "Connection string not found in pglister.ini"
            sys.exit(1)

    def conn(self, appname):
        if not self._conn:
            self._conn = psycopg2.connect(self.connstr + ' application_name=' + appname)
        return self._conn

    def get(self, *args, **kwargs):
        return self.cfg.get(*args, **kwargs)

    def getint(self, *args, **kwargs):
        return self.cfg.getint(*args, **kwargs)

    def get_default(self, section, option, default):
        try:
            return self.cfg.get(section, option)
        except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
            return default


# Global variable and the thing we actually use
config = Config()
