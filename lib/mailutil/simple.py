from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.Utils import formataddr, formatdate, make_msgid
from email.header import Header


def encoded_email_header(name, email):
    return formataddr((
        str(Header(name, 'utf-8')),
        email))


def make_simple_mail(senderaddr, sendername, recipientaddr, recipientname, subject, body, headers=None):
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['To'] = encoded_email_header(recipientname, recipientaddr)
    msg['From'] = encoded_email_header(sendername, senderaddr)
    msg['Date'] = formatdate(localtime=True)
    msg['Message-ID'] = make_msgid()
    if headers:
        for k, v in headers.items():
            msg[k] = v

    msg.attach(MIMEText(body, _charset='utf-8'))

    return msg
