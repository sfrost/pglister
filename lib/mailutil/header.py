from email.header import decode_header
from email.errors import HeaderParseError

from mailutil.charset import clean_charset


def decode_mime_header(hdr):
    try:
        return " ".join([unicode(s, charset and clean_charset(charset) or 'us-ascii', errors='ignore') for s, charset in decode_header(hdr)])
    except HeaderParseError:
        return unicode(hdr, 'us-ascii', errors='ignore')
    except:
        return "Unparsable header"
