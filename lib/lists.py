import re
from config import config
from misc import log


class ModerationLevels(object):
    NONE = 0
    SUBSCRIBERS = 1
    GLOBAL = 2
    ALL = 3
    DOUBLE = 4

    choices = (
        (NONE, 'No moderation'),
        (SUBSCRIBERS, 'Direct subscribers can post'),
        (GLOBAL, 'Global subscribers can post'),
        (ALL, 'All emails are moderated'),
        (DOUBLE, 'All emails are double-moderated'),
    )


class SubscriptionPolicies(object):
    OPEN = 0
    CONFIRMED = 1
    MANAGED = 2

    choices = (
        (OPEN, 'Anybody can subscribe'),
        (CONFIRMED, 'Requires moderator approval'),
        (MANAGED, 'Automatically managed'),
    )


class CcPolicies(object):
    ALLOW = 0
    DISCARD = 1

    choices = (
        (ALLOW, 'Allow posts when multiple lists are in to/cc'),
        (DISCARD, 'Discard posts if multiple lists are in to/cc'),
    )


class BccPolicies(object):
    ALLOW = 0
    MODERATE = 1
    DISCARD = 2

    choices = (
        (ALLOW, 'Allow posts even when list is not in to/cc'),
        (MODERATE, 'Moderate posts when list is not in to/cc'),
        (DISCARD, 'Discard posts when list is not in to/cc'),
    )


class ModerationReason(object):
    OTHER = 0
    SENDER_NOTSUBSCRIBER_LIST = 1
    SENDER_NOTSUBSCRIBER_ANY = 2
    ALL_POSTS_HELD = 3
    SENDER_BLACKLISTED = 4
    SIZE = 5
    SPAM = 6
    UNSUBSCRIBE = 7
    MODERATELIST = 8
    MODERATEREGEX = 9
    TAGERROR = 10
    BCC = 11
    GLOBALMODERATEREGEX = 12

    _messages = {
        OTHER: 'Other reason: {0}',
        SENDER_NOTSUBSCRIBER_LIST: 'sender is not a subscriber of the list',
        SENDER_NOTSUBSCRIBER_ANY: 'sender is not a subscriber of any list',
        ALL_POSTS_HELD: 'all posts held for moderation',
        SENDER_BLACKLISTED: 'sender address matches blacklist pattern',
        SIZE: '{0}',
        SPAM: '{0}',
        UNSUBSCRIBE: 'message looks like a possible unsubscription request ({0})',
        MODERATELIST: 'referenced messageid {0} found on moderation list',
        MODERATEREGEX: 'message matches moderation regex {0}',
        TAGERROR: 'tag processing error: {0}',
        BCC: 'list name or aliases was not in to or cc field',
        GLOBALMODERATEREGEX: 'message matches global moderation rgex for {0}',
    }

    def __init__(self, reason, extra=''):
        self.reason = reason
        self.extra = extra

    def full_string(self):
        return self._messages[self.reason].format(self.extra)

    def get_moderation_options(self, is_subscribed, is_superuser):
        yield (1, "Approve")

        if self.reason in (self.SENDER_NOTSUBSCRIBER_LIST, self.SENDER_NOTSUBSCRIBER_ANY):
            yield (2, "Whitelist")

        yield (3, "Discard")
        yield (4, "Reject")

        if is_subscribed and self.reason == self.UNSUBSCRIBE:
            yield (5, "Unsubscribe")

        if is_superuser and self.reason in (self.SENDER_NOTSUBSCRIBER_LIST, self.SENDER_NOTSUBSCRIBER_ANY,
                                            self.SPAM, self.BCC):
            yield (6, "Blacklist")
            yield (8, "Blacklist domain")

        yield (7, "Reject unsub")


class MailingList(object):
    def __init__(self, conn, id, address, name, domain, moderation_level, moderation_regex, spamscore_threshold, maxsize, maxsizedrop, archive_server, archive_address, helppage, archive_domain, blacklist_regex, whitelist_regex, tagged_delivery, tagkey, taglistsource, send_moderation_notices, cc_policy, bcc_policy):
        """
        Internal, do not call directly! Instead use constructor methods:
        MailingList.get_by_address(address)
        """

        self.conn = conn
        self.id = id
        self.address = address
        self.name = name
        self.domain = domain
        self.moderation_level = moderation_level
        self.moderation_regexes = [re.compile(r, re.MULTILINE) for r in moderation_regex.splitlines()]
        self.spamscore_threshold = spamscore_threshold
        self.maxsize = maxsize
        self.maxsizedrop = maxsizedrop
        self.archive_server = archive_server
        self.archive_address = archive_address
        self.helppage = helppage
        self.archive_domain = archive_domain
        self.blacklist_regexes = [re.compile(r, re.MULTILINE) for r in blacklist_regex.splitlines()]
        self.whitelist_regexes = [re.compile(r, re.MULTILINE) for r in whitelist_regex.splitlines()]
        self.tagged_delivery = tagged_delivery
        self.tagkey = tagkey
        self.taglistsource = taglistsource
        self.send_moderation_notices = send_moderation_notices
        self.cc_policy = cc_policy
        self.bcc_policy = bcc_policy

        # Default spamscore
        if not self.spamscore_threshold:
            self.spamscore_threshold = int(config.get('defaults', 'spamscore_threshold'))

        # Default size limits
        if not maxsize:
            self.maxsize = int(config.get('defaults', 'size_moderate'))
        if not maxsizedrop:
            self.maxsizedrop = int(config.get('defaults', 'size_drop'))

    _SELECTFIELDS = "id, address, name, domain, moderation_level, moderation_regex, spamscore_threshold, maxsizemoderate, maxsizedrop, archiveserver, archiveaddress, helppage, archivedomain, blacklist_regex, whitelist_regex, tagged_delivery, tagkey, taglistsource, send_moderation_notices, cc_policy, bcc_policy"

    @staticmethod
    def get_by_address(conn, address):
        """
        Return a MailingList object representing a list with the specified
        email address.
        """

        with conn.cursor() as curs:
            curs.execute("SELECT {0} FROM mailinglists WHERE address=%(address)s".format(MailingList._SELECTFIELDS), {
                'address': address,
            })
            rr = curs.fetchall()
            if len(rr) == 1:
                return MailingList(conn, *rr[0])
            else:
                return None

    @staticmethod
    def get_by_id(conn, id):
        """
        Return a MailingList object representing a list with the specified id.
        """

        with conn.cursor() as curs:
            curs.execute("SELECT {0} FROM mailinglists WHERE id=%(id)s".format(MailingList._SELECTFIELDS), {
                'id': id,
            })
            rr = curs.fetchall()
            if len(rr) == 1:
                return MailingList(conn, *rr[0])
            else:
                return None

    def moderation_reason(self, sender, messageid):
        """
        Check if a sender requires moderation to post to this list, and if
        so, return the reason why moderation is needed. If moderation
        is not needed, return None.

        """
        curs = self.conn.cursor()

        # If there are whitelists or blacklists, scan those first
        if self.whitelist_regexes:
            for r in self.whitelist_regexes:
                if r.search(sender):
                    # Found on whitelist, so no moderation on this
                    # address.
                    log(curs, 0, 'mail',
                        'Mail from {0} matched whitelist pattern, posting without moderation'.format(sender),
                        messageid)
                    return None

        if self.blacklist_regexes:
            for r in self.blacklist_regexes:
                if r.search(sender):
                    # Found in blacklist
                    return ModerationReason(ModerationReason.SENDER_BLACKLISTED)

        # No match on white or blacklists, so move on to regular policies
        if self.moderation_level == ModerationLevels.NONE:
            return None
        if self.moderation_level == ModerationLevels.SUBSCRIBERS:
            # Check if sender is a subscriber to this list
            curs.execute("SELECT EXISTS (SELECT 1 FROM mailinglist_subscribers WHERE listid=%(id)s AND email=%(email)s) OR EXISTS (SELECT 1 FROM mailinglist_whitelist WHERE listid=%(id)s AND email=%(email)s) OR EXISTS (SELECT 1 FROM mailinglist_globalwhitelist WHERE email=%(email)s)", {
                'id': self.id,
                'email': sender,
            })
            if not curs.fetchone()[0]:
                return ModerationReason(ModerationReason.SENDER_NOTSUBSCRIBER_LIST)
            return None
        if self.moderation_level == ModerationLevels.GLOBAL:
            # Check if the sender has a registered and verified email address
            # on this system (regardless of if it's subscribed to anything)
            # Also check the per-list whitelist!
            curs.execute("SELECT EXISTS (SELECT 1 FROM lists_subscriberaddress sa WHERE email=%(email)s AND sa.confirmed AND NOT sa.blacklisted) OR EXISTS (SELECT 1 FROM mailinglist_globalwhitelist WHERE email=%(email)s) OR EXISTS (SELECT 1 FROM mailinglist_whitelist WHERE listid=%(id)s AND email=%(email)s)", {
                'id': self.id,
                'email': sender,
            })
            if not curs.fetchone()[0]:
                return ModerationReason(ModerationReason.SENDER_NOTSUBSCRIBER_ANY)
            return None
        if self.moderation_level == ModerationLevels.ALL:
            return ModerationReason(ModerationReason.ALL_POSTS_HELD)
        if self.moderation_level == ModerationLevels.DOUBLE:
            return ModerationReason(ModerationReason.ALL_POSTS_HELD)
        return ModerationReason(ModerationReason.OTHER, "Unknown")

    def get_moderators(self):
        """
        Return a list of (email, name) combinations for all moderators of this list.
        """

        curs = self.conn.cursor()
        curs.execute("SELECT email, name FROM mailinglist_moderators WHERE listid=%(id)s", {
            'id': self.id,
        })
        return curs.fetchall()

    def writeheaders(self, buf):
        """
        Write the appropriate RFC2369 and RFC2919 headers to the
        buffer at the current location.
        """
        buf.write("List-Id: <{0}>\r\n".format(self.address.replace('@', '.')))
        buf.write("List-Help: <{0}>\r\n".format(self.helppage))
        buf.write("List-Subscribe: <{0}>\r\n".format(self.helppage))
        buf.write("List-Post: <mailto:{0}>\r\n".format(self.address))
        buf.write("List-Owner: <mailto:{0}>\r\n".format(self.owner_address()))
        if self.archive_address:
            buf.write("List-Archive: <{0}>\r\n".format(self.archive_address))
        buf.write("Precedence: bulk\r\n")

    def owner_address(self):
        """
        Return the owner address for this list. Will automatically be on
        the format where:
        llll@domain.com -> llll-owner@domain.com
        """
        return self.address.replace('@', '-owner@')

    def owner_name(self):
        """
        Return a name for the owner of this list. Will automatically be on
        the format where:
        lll@domain.com -> "lll owner"
        """
        return u"{0} Owner".format(self.name)

    def archive_submit_address(self):
        """
        Return the email address to submit to the archives for this list.
        """
        if self.archive_domain:
            # We only support a flat namespace per server, so just put
            # the list name and the server name.
            return "{0}@{1}".format(self.name, self.archive_domain)
        else:
            return None

    def moderator_notice_address(self):
        """
        Return address used to send moderation notices. For now, we just
        return the owner address.
        """
        return self.owner_address()

    def moderator_notice_name(self):
        """
        Return name used to send moderation notices. For now, we just
        return the owner name.
        """
        return self.owner_name()

    def unsubscribe_address(self):
        """
        Return address that will initiate an unsubscribe request to this
        list. This request will still need to be double-verified, but
        it's good manners to allow unsubscribe via email.

        This address needs to be list-global because we don't change
        the *contents* of the email between the recipients, only the
        VERP. Thus, use the llll-unsubscribe@domain.com format, which
        will generate an automatic confirmation email.
        """
        return self.address.replace('@', '-unsubscribe@')
