#!/usr/bin/python -u
#
# mailprocessor.py - main mail processor for regular (non-bounce) mails
#
# This script is intended to be run as a daemon.
#


import os
import sys
import select

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from config import config
from handlers.mailqueuehandler import MailQueueHandler

if __name__ == "__main__":
    conn = config.conn('mailprocessor')
    curs = conn.cursor()

    curs.execute("LISTEN incoming")
    conn.commit()

    while True:
        # Process everything that's sitting in the incoming queue. We
        # process one message per transaction, for reasonable recovery
        # levels if there are a lot of things in the queue.
        while True:
            handler = MailQueueHandler(conn)
            if not handler.process_next():
                break

        # Wait for activity for up to 5 minutes. If nothing happened,
        # then we run the process *anyway*, just in case we missed a
        # notify somewhere.
        select.select([conn], [], [], 5 * 60)

        # Eat up all notifications, since we're just going to process
        # all pending messages until the queue is empty.
        conn.poll()
        while conn.notifies:
            conn.notifies.pop()

        # Loop back up and process the full queue
