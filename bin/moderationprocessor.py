#!/usr/bin/python -u
#
# moderationprocessor.py - processor for reinjecting moderated emails
#
# This script is intended to be run as a daemon.
#


import os
import sys
import select

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from config import config
from handlers.moderationqueuehandler import ModerationQueueHandler
from handlers.moderationnoticesender import ModerationNoticeSender

if __name__ == "__main__":
    conn = config.conn('moderationprocessor')
    curs = conn.cursor()

    curs.execute("LISTEN moderation")
    conn.commit()

    while True:
        # Process everything that's sitting in the moderation queue. We
        # process one message per transaction, for reasonable recovery
        # levels if there are a lot of things in the queue.
        while True:
            handler = ModerationQueueHandler(conn)
            if not handler.process_next():
                break

        # Then queue any pending moderation notices for sending
        while True:
            handler = ModerationNoticeSender(conn)
            if not handler.process_next():
                break

        # Wait for activity for up to 30 seconds. If nothing happened,
        # then we run the process *anyway*, just in case we missed a
        # notify somewhere. We check as often as 30 seconds in order
        # to trigger outgoing moderation notices quickly enough.
        select.select([conn], [], [], 30)

        # Eat up all notifications, since we're just going to process
        # all pending messages until the queue is empty.
        conn.poll()
        while conn.notifies:
            conn.notifies.pop()

        # Loop back up and process the full queue
