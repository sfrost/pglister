#!/usr/bin/python -u
#
# mailsender.py - send outgoing mails to the MTA
#
# This script will run multiple parallel sessions based on the
# configuration parameter sender.sessions, each will connect
# to the server at sender.smtpaddr and inject messages.

# All sessions will run independently and "compete" for the same
# slots in the server, and we'll use locking on the server to
# keep them apart. For the time being we process both admin
# and regular queues in the same worker pool.
#
# This script is intended to be run as a daemon.
#


import os
import sys
import select
import time
import multiprocessing
import smtplib

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from config import config


class SmtpWrapper(object):
    def __init__(self, batchsize, smtpserver, heloname):
        self.batchsize = batchsize
        self.smtpserver = smtpserver
        self.heloname = heloname
        self.smtp = None
        self.counter = 0

    def close(self):
        if self.smtp:
            self.smtp.close()
            self.smtp = None
        self.counter = 0

    def send(self, sender, recipient, contents):
        # If we've gone beyond our batch size, then close the connection
        self.counter += 1
        if self.counter > self.batchsize:
            self.close()

        # If we just closed the connection, or haven't opened one yet, get a new one
        if not self.smtp:
            self.smtp = smtplib.SMTP(self.smtpserver, local_hostname=self.heloname)

        # Now send it
        try:
            self.smtp.sendmail(sender, recipient, contents)
        except Exception, e:
            print "Failed to send: %s" % e
            return False
        return True


def _send_from_query(smtp, curs, query):
    # For now we always fetch a single entry from the database, but batch them to SMTP.
    # In case something goes wrong we don't want to deliver mails multiple times because
    # a full transaction rolled back.
    # Transaction is automatically started by psycopg2 at the first execute.
    curs.execute(query)
    r = curs.fetchone()
    if not r:
        # Nothing left in the queue. Our work here is done.
        return False

    return smtp.send(r[0], r[1], r[2])


def process_queue(conn, batchsize, smtpserver, heloname):
    # Start by processing the admin queue if there is anything in there
    curs = conn.cursor()

    smtp = SmtpWrapper(batchsize, smtpserver, heloname)

    while True:
        if not _send_from_query(smtp, curs, "DELETE FROM admin_out WHERE id=(SELECT id FROM admin_out ORDER BY id FOR UPDATE SKIP LOCKED LIMIT 1) RETURNING sender, recipient, contents"):
            conn.rollback()
            break
        else:
            conn.commit()

    while True:
        if not _send_from_query(smtp, curs, "DELETE FROM raw_out WHERE id=(SELECT id FROM raw_out ORDER BY id FOR UPDATE SKIP LOCKED LIMIT 1) RETURNING sender, recipient, contents"):
            conn.rollback()
            break
        else:
            conn.commit()

    while True:
        if not _send_from_query(smtp, curs, "WITH r AS (DELETE FROM outgoing_recipients WHERE id=(SELECT id FROM outgoing_recipients ORDER BY id FOR UPDATE SKIP LOCKED LIMIT 1) RETURNING id, outgoing_id, subscriberaddress_id, recipient_headers) SELECT verp_sender(sendinglist, subscriberaddress_id, outgoing_id), email, headers || convert_to(recipient_headers,'utf8') || chr(13)::bytea || chr(10)::bytea || body FROM r INNER JOIN outgoing o ON o.id=r.outgoing_id INNER JOIN lists_subscriberaddress sa ON sa.id=r.subscriberaddress_id"):
            conn.rollback()
            break
        else:
            conn.commit()

    smtp.close()


def worker():
    conn = config.conn('mailsender')
    curs = conn.cursor()

    curs.execute("LISTEN admin_out")
    curs.execute("LISTEN outgoing")
    curs.execute("LISTEN raw_out")
    conn.commit()

    while True:
        # Initially process the queue so we start sending things
        # immediately on startup. Once that is done, wait for
        # notifications.
        process_queue(
            conn,
            int(config.get('sender', 'batchsize')),
            config.get('sender', 'smtpaddr'),
            config.get('sender', 'heloname'),
        )

        # Wait for activity for up to 3 minutes. If nothing happened,
        # then we run the process *anyway*, just in case we missed a
        # notify somewhere.
        select.select([conn], [], [], 3 * 60)

        # Eat up all notifications, since we're just going to process
        # all pending messages until the queue is empty.
        conn.poll()
        while conn.notifies:
            conn.notifies.pop()

        # Loop back up and process the full queue


if __name__ == "__main__":
    processes = {}
    sessioncount = int(config.get('sender', 'sessions'))
    for i in range(sessioncount):
        p = multiprocessing.Process(target=worker, args=())
        p.start()
        processes[p.pid] = p

    # Workers should never die. If they do, commit suicide and
    # restart everything.
    while True:
        # We can't use os.wait(), because it interferes with the join/is_alive
        # functionality in the multiprocess module. So just hang around and make
        # a check every 10 seconds - we don't need to react quicker than that.
        time.sleep(10)

        for pid, p in processes.items():
            # Join with no timeout to update the alive state
            p.join(0)

            if not p.is_alive():
                print "Child process {0} died, respawning".format(pid)
                del processes[pid]
                p = multiprocessing.Process(target=worker, args=())
                p.start()
                processes[p.pid] = p
