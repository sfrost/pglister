#!/usr/bin/env python

import sys
import os
import argparse

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))
sys.path.append('/usr/local/pglister/lib')

from config import config


def yesno(p):
    while True:
        r = raw_input(p).lower()
        if r == 'y' or r == 'yes':
            return True
        if r == 'n' or r == 'no':
            return False


def log(curs, msg, msgid=None):
    curs.execute("INSERT INTO log (level, source, msg, username, messageid) VALUES (0, 'cli', %(msg)s, %(user)s, %(msgid)s)", {
        'msg': msg,
        'msgid': msgid,
        'user': os.getlogin(),
    })


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="pglister cli")
    parser.add_argument('command', choices=['unregister', ], help='What to do')
    parser.add_argument('--email', type=str, help='Email address to operate on')
    parser.add_argument('--noexim', action='store_true', help='Disable any operations that use exim (and require root)')

    args = parser.parse_args()

    do_exim = False

    if args.command == 'unregister':
        if not args.email:
            print "Email must be specified for unregister"
            sys.exit(1)
        do_exim = True

    if args.noexim:
        do_exim = False

    if do_exim:
        # Talking to exim requires root
        if os.geteuid() != 0:
            print "Using exim functionality requires root. Either re-run command as root"
            print "or run with --noexim"
            sys.exit(1)

    try:
        conn = config.conn('pglistercli')
    except Exception, e:
        print "Failed to connect to database: {0}".format(e)
        print "Perhaps you are not running as a user with access to the db?"
        sys.exit(1)

    curs = conn.cursor()

    if args.command == 'unregister':
        curs.execute("SELECT id FROM lists_subscriberaddress WHERE email=%(email)s", {
            'email': args.email.lower(),
        })
        if curs.rowcount != 1:
            print "Email {0} not found.".format(args.email.lower())
            sys.exit(1)
        subid = curs.fetchone()[0]

        curs.execute("WITH t(list_id) AS (DELETE FROM lists_listsubscription WHERE subscriber_id=%(subid)s RETURNING list_id) SELECT name FROM lists_list INNER JOIN t ON lists_list.id=t.list_id", {
            'subid': subid,
        })
        if curs.rowcount:
            print "Removed subscription from lists:"
            for l, in curs.fetchall():
                print l
        else:
            print "Address found, but not subscribed to any list"
        curs.execute("DELETE FROM lists_subscriberaddress WHERE id=%(subid)s", {
            'subid': subid,
        })
        if curs.rowcount != 1:
            print "Failed to remove subscriberaddress"
            sys.exit(1)
        print "Removed subscriberaddress for {0}".format(args.email.lower())
        log(curs, "Removed subscriber {0} from all lists".format(args.email.lower()))
        if yesno('OK to commit this to database?'):
            conn.commit()
            print "Committed."
        else:
            conn.rollback()
            print "Changes rolled back."
            sys.exit(0)

        if do_exim:
            print "Removing mails to {0} from the queue by flagging them as delivered".format(args.email.lower())
            os.system("exiqgrep -ir {0} | xargs -I% exim -Mmd % {0}".format(args.email.lower()))

    print "Done!"
    conn.close()
