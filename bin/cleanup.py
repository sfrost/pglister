#!/usr/bin/python -u
#
# cleanup.py - run scheduled cleanups. Basically a "builtin cron"
#
# This script is intended to be run as a daemon.
#


import os
import sys
import time

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from config import config
from handlers.cleanuphandler import CleanupHandler

if __name__ == "__main__":
    conn = config.conn('cleanup')
    curs = conn.cursor()

    while True:
        handler = CleanupHandler(conn)
        handler.process()

        time.sleep(5 * 60)
