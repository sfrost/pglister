#!/usr/bin/env python
#
# pglister_queues.py - nagios monitor for contents of pglister queues
#
#
# To override the values for warning and critical, add to pglister.ini
# something like:
#
# [nagios]
# incoming_warn=10
# bounces_failed_crit=2
#


import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from config import config

LEVEL_OK = 0
LEVEL_WARNING = 1
LEVEL_CRITICAL = 2
LEVELSTR = {
    LEVEL_OK: 'OK',
    LEVEL_WARNING: 'WARNING',
    LEVEL_CRITICAL: 'CRITICAL',
}


class Aggregator(object):
    def __init__(self):
        self.worst_level = LEVEL_OK
        self.issues = []

    def levelup(self, level):
        if level > self.worst_level:
            self.worst_level = level

    def addif(self, counter, threshold_param, def_warn, def_crit, whatis):
        threshold_warn = int(config.get_default('nagios', '{0}_warn'.format(threshold_param), def_warn))
        threshold_crit = int(config.get_default('nagios', '{0}_crit'.format(threshold_param), def_crit))
        if counter >= threshold_warn:
            if counter >= threshold_crit:
                threshold = threshold_crit
                self.levelup(LEVEL_CRITICAL)
            else:
                threshold = threshold_warn
                self.levelup(LEVEL_WARNING)
            self.issues.append("{0} is {1}, threshold {2}".format(whatis, counter, threshold))

    def print_and_return(self):
        print "{0}: {1}".format(LEVELSTR[self.worst_level], " :: ".join(self.issues))
        return self.worst_level


if __name__ == "__main__":
    conn = config.conn('nagios_queues')
    curs = conn.cursor()

    # Incoming queue
    curs.execute("SELECT count(*) FILTER (WHERE NOT failed), count(*) FILTER (WHERE failed) FROM incoming_mail")
    (incoming_ok, incoming_failed) = curs.fetchone()

    # Admin queue
    curs.execute("SELECT count(*) FROM admin_out")
    (admin_total,) = curs.fetchone()

    # Raw quuee
    curs.execute("SELECT count(*) FROM raw_out")
    (raw_total,) = curs.fetchone()

    # Outgoing queue (messages, not recipients)
    curs.execute("SELECT count(*) FROM outgoing")
    (outgoing_total,) = curs.fetchone()

    # Bounce queue
    curs.execute("SELECT count(*) FILTER (WHERE NOT failed), count(*) FILTER (WHERE failed) FROM bounce_mail")
    (bounces_ok, bounces_failed) = curs.fetchone()

    # Moderation queue
    curs.execute("SELECT count(*) FROM moderation WHERE approved")
    (moderation_approved, ) = curs.fetchone()

    conn.rollback()
    conn.close()

    a = Aggregator()
    a.addif(incoming_ok, 'incoming', 3, 7, "incoming queue")
    a.addif(incoming_failed, 'incoming_failed', 1, 1, "incoming failed queue")
    a.addif(admin_total, 'admin', 1, 5, "admin queue")
    a.addif(raw_total, 'raw', 1, 5, "raw queue")
    a.addif(outgoing_total, 'outgoing', 5, 10, "outgoing queue")
    a.addif(bounces_ok, 'bounces', 3, 7, "bounces queue")
    a.addif(bounces_failed, 'bounces_failed', 1, 1, "bounces failed queue")
    a.addif(moderation_approved, 'moderation', 1, 1, "approved moderations not delivered")

    sys.exit(a.print_and_return())
