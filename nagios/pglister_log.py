#!/usr/bin/env python
#
# pglister_log.py - nagios monitor for pglister log
#
# Monitors that there are no unconfirmed ERROR level log entries.

#

import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../lib')))

from config import config

if __name__ == "__main__":
    conn = config.conn('nagios_log')
    curs = conn.cursor()

    curs.execute('SELECT count(*) FROM log WHERE level=2 AND NOT confirmed')

    num, = curs.fetchone()
    conn.close()
    if num > 0:
        print "CRITICAL: {0} unconfirmed errors".format(num)
        sys.exit(2)
    print "OK, no unconfirmed errors"
    sys.exit(0)
