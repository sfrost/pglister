from django.db.models import Q
from django.db import transaction

import re

from lib.misc import generate_random_token

from models import Subscriber, SubscriberAddress


def handle_login(sender, user, request, **kwargs):
    """
    Gets called whenever a user logged in.

    We use this to migrate information from the old systems. This is
    done by simply matching up any existing subscriptions with the
    exact same email address, so they get added automatically.

    We can safely do this, because "upstream" community authentication
    has validated the ownership of the email address already.
    """

    (name, domain) = request.user.email.split('@')

    subaddrs = list(SubscriberAddress.objects.filter(
        Q(email=request.user.email) |
        Q(email__regex='^{0}\+[^@]+@{1}$'.format(re.escape(name), re.escape(domain))),
        subscriber__isnull=True)
    )
    if subaddrs:
        # If subaddrs has more than zero rows, it means we have one or
        # more unmatched subscriptions. So match them up to the current
        # user and store it.
        with transaction.atomic():
            if not hasattr(request.user, 'subscriber'):
                # Create a subscriber record for ourselves, that we can
                # then connect.
                s = Subscriber(user=request.user)
                s.save()
            else:
                s = request.user.subscriber

            for sa in subaddrs:
                sa.subscriber = s
                sa.save()
    else:
        # No existing subscriber addresses. However, we still need to
        # create a subscriber record for this user so the site works.
        # We also create a subscriber address for whatever email is on
        # the community account
        if not hasattr(request.user, 'subscriber'):
            with transaction.atomic():
                s = Subscriber(user=request.user)
                s.save()
                SubscriberAddress(subscriber=s, email=request.user.email, confirmed=True, token=generate_random_token()).save()
