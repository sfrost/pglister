from django.shortcuts import get_object_or_404, Http404
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.db import connection, transaction
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import View, TemplateView, FormView, RedirectView, UpdateView

from datetime import datetime

from models import List, Subscriber, SubscriberAddress
from models import ListSubscription, ListSubscriptionModerationToken
from forms import ConfirmForm, SubscribeForm, EditSubForm, AddMailForm, GlobalSettingsForm
from util import LoginRequired, send_template_mail, send_template_mail_to_moderators
from util import get_unsubscribe_instructions

from pglister.util import exec_to_dict, exec_to_scalar
from pglister.listlog.util import log, Level

from lib.lists import SubscriptionPolicies
from lib.misc import generate_random_token
from lib.mailutil.simple import make_simple_mail


class Home(TemplateView):
    template_name = 'home.html'


class Manage(LoginRequired, View):
    template_name = 'manage.html'

    def get_context_data(self, **kwargs):
        self.subscriber = get_object_or_404(Subscriber, user=self.request.user)

        modqueue = exec_to_dict("SELECT moderation_id, lists_list.name, sender, messageid, moderatedat FROM subscriber_moderation_queue INNER JOIN lists_list ON listid=lists_list.id WHERE subscriber_id=%(subid)s ORDER BY moderatedat LIMIT 20", {
            'subid': self.subscriber.pk,
        })
        is_moderator = exec_to_scalar("SELECT EXISTS (SELECT 1 FROM lists_list_moderators WHERE subscriber_id=%(userid)s)", {
            'userid': self.request.user.id,
        })
        has_subscriptions = exec_to_scalar("SELECT EXISTS (SELECT 1 FROM lists_listsubscription ls INNER JOIN lists_subscriberaddress a ON ls.subscriber_id=a.id WHERE a.subscriber_id=%(userid)s)", {
            'userid': self.request.user.id,
        })

        context = {
            'subscribers': SubscriberAddress.objects.filter(subscriber=self.subscriber),
            'has_subscriptions': has_subscriptions,
            'modqueue': modqueue,
            'is_moderator': is_moderator,
            'subscribeform': SubscribeForm(self.subscriber, True),
            'addmailform': AddMailForm(),
        }
        return context

    def get(self, request, *args, **kwargs):
        c = self.get_context_data()
        c['globalform'] = GlobalSettingsForm(instance=self.subscriber)
        return render(request, self.template_name, c)

    def post(self, request, *args, **kwargs):
        c = self.get_context_data()
        form = GlobalSettingsForm(data=request.POST, instance=self.subscriber)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('.')

        c['globalform'] = form
        return render(request, self.template_name, c)


class Unqueue(LoginRequired, FormView):
    template_name = 'baseform.html'
    success_url = '/manage/'
    form_class = ConfirmForm
    savebutton = 'Cancel mail'
    cancelurl = '/manage/'
    cancelname = 'Return without canceling'

    def get_form_kwargs(self):
        kw = super(Unqueue, self).get_form_kwargs()

        subscriber = get_object_or_404(Subscriber, user=self.request.user)

        m = exec_to_dict("SELECT messageid FROM subscriber_moderation_queue WHERE subscriber_id=%(subid)s AND moderation_id=%(modid)s", {
            'subid': subscriber.pk,
            'modid': self.kwargs['modid'],
        })
        if len(m) != 1:
            raise Http404("Mail not found in queue or no permissions")
        msgid = m[0]['messageid']

        kw['prompt'] = u"Please confirm that you want to remove the message with messageid {0} from the queue.".format(msgid)
        return kw

    @transaction.atomic
    def form_valid(self, form):
        subscriber = get_object_or_404(Subscriber, user=self.request.user)

        curs = connection.cursor()
        # The extra EXISTS query is to ensure the email addresses didn't change underneath us during
        # the form submit.
        curs.execute("DELETE FROM moderation WHERE id=%(modid)s AND EXISTS (SELECT 1 FROM subscriber_moderation_queue WHERE subscriber_id=%(subid)s AND moderation_id=%(modid)s) RETURNING messageid", {
            'subid': subscriber.pk,
            'modid': self.kwargs['modid'],
        })
        msgid = curs.fetchall()[0][0]

        log(Level.INFO, self.request.user, "Removed message from queue", msgid)

        messages.info(self.request, "Email removed from moderation queue")
        return super(Unqueue, self).form_valid(form)


class UnqueueToken(TemplateView):
    template_name = 'unqueue_token.html'

    @transaction.atomic
    def get(self, request, usertoken):
        msgid = exec_to_scalar("DELETE FROM moderation WHERE usertoken=%(usertoken)s RETURNING messageid", {
            'usertoken': usertoken,
        })

        if msgid:
            log(Level.INFO, self.request.user, "Removed message from queue by token", msgid)
            self.success = True
        else:
            self.success = False

        return super(UnqueueToken, self).get(request, usertoken)


class Subscribe(LoginRequired, FormView):
    template_name = 'baseform.html'
    success_url = '/manage/'
    form_class = SubscribeForm
    savebutton = 'Subscribe'
    cancelurl = '/manage/'

    def get_form_kwargs(self):
        kw = super(Subscribe, self).get_form_kwargs()
        kw['subscriber'] = get_object_or_404(Subscriber, user=self.request.user)
        return kw

    def _add_tags(self, subscription, l, taglist):
        if not l.tagged_delivery:
            return
        for t in taglist:
            subscription.tags.add(t)
        subscription.save()

    @transaction.atomic
    def form_valid(self, form):
        l = get_object_or_404(List, pk=form.cleaned_data['listid'])
        subscriber = get_object_or_404(Subscriber, user=self.request.user)

        if subscriber != form.cleaned_data['addr'].subscriber:
            raise Http404("Invalid subscriber on subscriber address")

        if l.subscription_policy == 2:
            # Internal list. Should be impossible to get here.
            raise Http404("Invalid list policy")

        if l.subscription_policy == 1:
            # Subscription requires moderation
            s = ListSubscription(list=l, subscriber=form.cleaned_data['addr'], subscription_confirmed=False)
            s.save()
            self._add_tags(s, l, form.cleaned_data.get('tags', []))
            t = ListSubscriptionModerationToken(subscription=s,
                                                tokensent=datetime.now(),
                                                token=generate_random_token())
            t.save()

            # Generate email to moderators
            modstr = u"Sender: {0}\nList: {1}\n\nApprove: {2}/listmoderate/{3}/approve/\nDiscard: {2}/listmoderate/{3}/discard/\nReject: {2}/listmoderate/{3}/reject/\n".format(
                form.cleaned_data['addr'].email,
                l,
                settings.WEB_ROOT,
                t.token,
            )
            mods = list(l.moderators.all())
            curs = connection.cursor()
            for m in mods:
                curs.execute("INSERT INTO admin_out (sender, recipient, contents) VALUES (%(sender)s, %(recipient)s, %(contents)s)", {
                    'sender': l.moderator_notice_address(),
                    'recipient': m.user.email,
                    'contents': make_simple_mail(l.moderator_notice_address(),
                                                 l.moderator_notice_name(),
                                                 m.user.email,
                                                 m.fullname,
                                                 u"Attempted subscription to {0}".format(l),
                                                 modstr).as_string(),
                })

            # Send NOTIFY for mailsender to pick up on
            curs.execute("NOTIFY admin_out")

            # Log what happened, and let the user know
            log(Level.INFO, self.request.user, u"Requested subscription to {0} using {1}. Request has been sent to {2} moderators.".format(l, form.cleaned_data['addr'].email, len(mods)))
            messages.info(self.request, u"Your request for subscription to {0} has been registered, and your subscription will be completed as soon as it has been approved by a moderator.".format(l))
        elif l.subscription_policy == 0:
            # Public subscription
            s = ListSubscription(list=l, subscriber=form.cleaned_data['addr'])
            s.save()
            self._add_tags(s, l, form.cleaned_data.get('tags', []))

            log(Level.INFO, self.request.user, u"Subscribed to {0} using {1}.".format(l, form.cleaned_data['addr'].email))
            messages.info(self.request, u"You have been subscribed to {0}".format(l))
            if l.notify_subscriptions:
                send_template_mail_to_moderators(l,
                                                 'mail/subscription_notify.txt',
                                                 u'Subscription to {0}'.format(l),
                                                 {'email': form.cleaned_data['addr'].email},
                                                 )
        else:
            raise Exception("Cannot happen")

        return super(Subscribe, self).form_valid(form)


class Unsubscribe(LoginRequired, FormView):
    template_name = 'baseform.html'
    success_url = '/manage/'
    form_class = ConfirmForm
    savebutton = 'Unsubscribe'
    cancelurl = '/manage/'

    def get_form_kwargs(self):
        kw = super(Unsubscribe, self).get_form_kwargs()

        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        subaddr = get_object_or_404(SubscriberAddress, pk=self.kwargs['subid'], subscriber=subscriber)
        l = get_object_or_404(List, pk=self.kwargs['listid'])

        kw['prompt'] = u"Please confirm that you want to unsubscribe from {0}.".format(l)
        if l.subscription_policy == SubscriptionPolicies.MANAGED:
            kw['cannotcomplete'] = "This list has it's membership automatically managed, so you cannot manually unsubscribe from it."
        return kw

    @transaction.atomic
    def form_valid(self, form):
        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        subaddr = get_object_or_404(SubscriberAddress, pk=self.kwargs['subid'], subscriber=subscriber)
        l = get_object_or_404(List, pk=self.kwargs['listid'])

        if l.subscription_policy == 2:
            # Managed list, cannot unsubscribe. But it should be impossible
            # to get this far, so just show an error and redirect.
            messages.error(self.request, u"The list {0} is a managed list, and cannot be manually unsubscribed from.".format(l))
            return super(Unsubscribe, self).form_valid(form)

        ls = get_object_or_404(ListSubscription, list=l, subscriber=subaddr)

        ls.delete()

        if l.notify_subscriptions:
            send_template_mail_to_moderators(l,
                                             'mail/unsubscription_notify.txt',
                                             u'Unsubscription from {0}'.format(l),
                                             {'email': subaddr.email},
                                             )

        log(Level.INFO, self.request.user, u"Unsubscribed from {0}".format(l))
        messages.info(self.request, u"You have been unsubscribed from {0}.".format(l))

        return super(Unsubscribe, self).form_valid(form)


class UnsubscribeLink(TemplateView):
    template_name = 'unsubscribe_link.html'
    # Handle unsubscriptions clicked from the List-Unsubscribe header. For
    # this we do double-confirm, so send an email to confirm.

    def get_context_data(self, **kwargs):
        context = super(UnsubscribeLink, self).get_context_data(**kwargs)

        context['subaddr'] = get_object_or_404(SubscriberAddress, token=kwargs['token'])
        context['list'] = get_object_or_404(List, id=kwargs['listid'])

        return context

    @transaction.atomic
    def post(self, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        l = ctx['list'].wrapper
        sa = ctx['subaddr']
        if sa.subscriber:
            fullname = sa.subscriber.fullname
        else:
            fullname = None

        # Create an unsubscription token if not already there
        curs = connection.cursor()
        unsubtoken = generate_random_token()
        curs.execute("INSERT INTO unsubscribe_tokens (listid, subscriberaddress, token) VALUES (%(listid)s, %(subscriberaddress)s, %(token)s)", {
            'listid': l.id,
            'subscriberaddress': sa.id,
            'token': unsubtoken,
        })

        send_template_mail(
            'mail/unsubscribe_request.txt',
            l.owner_address(),
            l.owner_name(),
            sa.email,
            fullname,
            "Unsubscription request",
            {
                'name': l.name,
                'token': unsubtoken,
                'webroot': settings.WEB_ROOT,
            }
        )
        ctx['done'] = True
        return self.render_to_response(ctx)


class UnsubscribeConfirm(TemplateView):
    template_name = 'unsubscribe_confirm.html'

    # Handle unsubscription via link through email, and thus specifically *don't* require
    # a login.
    @transaction.atomic
    def get(self, request, token):
        curs = connection.cursor()
        # Token is stored "outside the django world"
        curs.execute("DELETE FROM unsubscribe_tokens WHERE token=%(token)s RETURNING listid,subscriberaddress", {
            'token': token,
        })
        r = curs.fetchone()
        if not r:
            self.message = "Unsubscription token not found. It may have expired."
            return super(UnsubscribeConfirm, self).get(request, token)

        # Ok, we have it. Now can we find a subscription for it?
        try:
            l = List.objects.get(id=r[0])
            s = ListSubscription.objects.get(list_id=r[0], subscriber=r[1])
        except List.DoesNotExist:
            self.message = "List no longer exists."
            return super(UnsubscribeConfirm, self).get(request, token)
        except ListSubscription.DoesNotExist:
            self.message = "Address has already been unsubscribed from this list."
            return super(UnsubscribeConfirm, self).get(request, token)

        # Check subscription policy
        if l.subscription_policy == SubscriptionPolicies.MANAGED:
            self.message = "This list has it's membership automatically managed, so you cannot manually unsubscribe from it."
            return super(UnsubscribeConfirm, self).get(request, token)
        # Ok, actually remove it.
        addr = s.subscriber.email
        s.delete()
        self.message = "Address {0} now unsubscribed from {1}.".format(addr, l)

        log(Level.INFO, self.request.user, u"Unsubscribed {0} from {1}".format(addr, l))

        return super(UnsubscribeConfirm, self).get(request, token)


class EditSub(LoginRequired, UpdateView):
    model = ListSubscription
    template_name = 'baseform.html'
    success_url = '/manage/'
    form_class = EditSubForm
    cancelurl = '/manage/'

    def get_object(self, queryset=None):
        o = super(EditSub, self).get_object(queryset)
        # We have to do this check "backwards", because o.subscriber.subscriber can be None.
        # If it is, nobody is allowed to edit.
        if o.subscriber.subscriber:
            if o.subscriber.subscriber.user == self.request.user:
                return o
        raise Http404()


class ViewMembership(LoginRequired, TemplateView):
    template_name = "view_membership.html"

    def get(self, request, listid):
        l = get_object_or_404(List, pk=listid, members_view_membership=True)
        subaddr = get_object_or_404(SubscriberAddress,
                                    subscriber__user=request.user,
                                    list=l)

        c = self.get_context_data()
        c['list'] = l
        c['members'] = l.subscribers.select_related('subscriber').all()
        return render(request, self.template_name, c)


class Testmail(LoginRequired, RedirectView):
    permanent = False
    url = '/manage/'

    def get(self, request, *args, **kwargs):
        subscriber = get_object_or_404(Subscriber, user=self.request.user)
        subaddr = get_object_or_404(SubscriberAddress, pk=self.kwargs['subid'], subscriber=subscriber)
        l = get_object_or_404(List, pk=self.kwargs['listid'])

        send_template_mail(
            'mail/test_mail.txt',
            l.owner_address(),
            l.owner_name(),
            subaddr.email,
            subscriber.fullname,
            "Test mail for {0}".format(l.name),
            {
                'list': l,
            },
            headers={
                'List-Id': '<{0}>'.format(unicode(l).replace('@', '.')),
            }
        )

        messages.info(self.request, "Test message sent")

        return super(Testmail, self).get(request, *args, **kwargs)


class UnsubscribeHelp(TemplateView):
    template_name = 'unsubscribe_help.html'

    def get_context_data(self, **kwargs):
        ctx = super(UnsubscribeHelp, self).get_context_data(**kwargs)
        ctx['client_instructions'] = get_unsubscribe_instructions()
        return ctx
