# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0010_notify_subscriptions'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='apikey_ro',
            field=models.CharField(max_length=100, null=True, verbose_name=b'Read-only API key', blank=True),
        ),
    ]
