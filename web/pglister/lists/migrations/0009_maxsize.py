# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pglister.lists.sizefield


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0008_whitelist_dates'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='maxsizedrop',
            field=pglister.lists.sizefield.SizeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='list',
            name='maxsizemoderate',
            field=pglister.lists.sizefield.SizeField(null=True, blank=True),
        ),
    ]
