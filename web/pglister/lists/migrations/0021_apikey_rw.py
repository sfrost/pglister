# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0020_archiveserver_api_key'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='apikey_rw',
            field=models.CharField(max_length=100, verbose_name=b'Read-write API key', blank=True),
        ),
    ]
