# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import pglister.lists.models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0018_lowercase_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscriberaddress',
            name='token',
            field=models.TextField(null=True),
        ),
        migrations.RunSQL(
            "UPDATE lists_subscriberaddress SET token=encode(pgcrypto.digest(pgcrypto.gen_random_bytes(250), 'sha256'), 'hex') WHERE token IS NULL",
        ),
        migrations.AlterField(
            model_name='subscriberaddress',
            name='token',
            field=models.TextField(null=False, blank=False, unique=True),
        ),
    ]
