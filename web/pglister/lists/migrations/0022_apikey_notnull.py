# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0021_apikey_rw'),
    ]

    operations = [
        migrations.AlterField(
            model_name='archiveserver',
            name='apikey',
            field=models.CharField(default='', max_length=100, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='list',
            name='apikey_ro',
            field=models.CharField(default='', max_length=100, verbose_name=b'Read-only API key', blank=True),
            preserve_default=False,
        ),
    ]
