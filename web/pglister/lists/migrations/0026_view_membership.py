# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0025_optional_moderation_notices'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='members_view_membership',
            field=models.BooleanField(default=False, help_text=b'Can members view the membership of the list?'),
        ),
    ]
