# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0026_view_membership'),
    ]

    operations = [
        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS log (
  id bigserial not null primary key,
  t timestamptz not null default current_timestamp,
  level int not null,
  source varchar(10) not null,
  username varchar(30) null,
  messageid varchar(1000) null,
  msg varchar(1000) not null,
  confirmed boolean not null default 'f'
)"""),
        migrations.RunSQL("CREATE INDEX IF NOT EXISTS log_t_idx ON log(t)"),
        migrations.RunSQL("CREATE INDEX IF NOT EXISTS idx_log_unconfirmed_level ON log (level) WHERE NOT confirmed"),
        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION trg_log_confirm_non_error()
RETURNS trigger
AS $$
BEGIN
   if NEW.level != 2 then
      NEW.confirmed := true;
   end if;
   RETURN NEW;
END;
$$ language plpgsql"""),
        migrations.RunSQL("DROP TRIGGER IF EXISTS trg_log_confirm_non_error ON log"),
        migrations.RunSQL("""
CREATE TRIGGER trg_log_confirm_non_error
BEFORE INSERT ON log
FOR EACH ROW EXECUTE PROCEDURE trg_log_confirm_non_error();
"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS incoming_mail(
  id bigserial not null primary key,
  recipient text not null,
  sender text not null,
  messageid text not null,
  dt timestamptz not null default current_timestamp,
  contents bytea not null,
  failed boolean not null default 'f'
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS bounce_mail(
  id bigserial not null primary key,
  recipient text not null,
  sender text not null,
  messageid text not null,
  dt timestamptz not null default current_timestamp,
  contents bytea not null,
  failed boolean not null default 'f'
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS matched_bounces(
  id bigserial not null primary key,
  list int not null,
  subscriber int not null,
  deliveryid bigint not null,
  dt timestamptz not null,
  messageid text not null,
  header text not null,
  message text not null,
  transient boolean not null default 'f'
)"""),
        migrations.RunSQL("CREATE INDEX IF NOT EXISTS matched_bounces_dt_idx ON matched_bounces(dt)"),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS unmatched_bounces(
  id bigserial not null primary key,
  recipient text not null,
  sender text not null,
  messageid text not null,
  dt timestamptz not null,
  contents text not null,
  reason text not null
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS moderation(
  id bigserial not null primary key,
  origid bigint not null,
  listid int not null,
  approved boolean not null default 'f',
  sender text not null,
  fromaddr text not null,
  recipient text not null,
  messageid text not null,
  moderatedat timestamptz not null default current_timestamp,
  previous_moderator int null,
  reason text not null,
  subject text not null,
  intro text not null,
  contents bytea not null,
  usertoken text UNIQUE not null
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS unsubscribe_tokens(
  id bigserial not null primary key,
  tokencreated timestamptz not null default current_timestamp,
  listid int not null,
  subscriberaddress bigint not null,
  token text not null unique
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS admin_out(
  id bigserial not null primary key,
  sender text not null,
  recipient text not null,
  contents text not null
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS outgoing(
  id bigserial not null primary key,
  queuedts timestamptz not null default current_timestamp,
  sendinglist text not null,
  messageid text not null,
  headers bytea not null,
  body bytea not null
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS outgoing_recipients(
  id bigserial not null primary key,
  outgoing_id bigint not null references outgoing(id),
  subscriberaddress_id int not null REFERENCES lists_subscriberaddress(id) ON DELETE CASCADE,
  recipient_headers text not null
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS outgoing_completed(
  id bigint not null primary key,
  queuedts timestamptz not null,
  completedts timestamptz not null,
  messageid text not null,
  sendinglist text not null
)"""),
        migrations.RunSQL("CREATE INDEX IF NOT EXISTS outgoing_completed_messageid_idx ON outgoing_completed(messageid)"),
        migrations.RunSQL("CREATE INDEX IF NOT EXISTS outgoing_completed_completed_idx ON outgoing_completed(completedts)"),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS raw_out(
  id bigserial not null primary key,
  sender text not null,
  recipient text not null,
  contents bytea not null
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS moderator_notices (
       id serial NOT NULL PRIMARY KEY,
       moderation_id int NOT NULL REFERENCES moderation(id) ON DELETE CASCADE,
       moderator_id int NOT NULL REFERENCES lists_list_moderators (id) ON DELETE CASCADE,
       sendat timestamptz NOT NULL,
       sent boolean not null default false,
       token text UNIQUE not null
)"""),


        migrations.RunSQL("""
CREATE TABLE IF NOT EXISTS matched_notice_bounces(
  id bigserial NOT NULL PRIMARY KEY,
  moderationid bigint NOT NULL,
  list text NOT NULL,
  usertoken text NOT NULL,
  bounce_dt timestamptz NOT NULL,
  bounce_msgid text NOT NULL,
  bounce_sender text NOT NULL,
  bounce_contents bytea
)"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglists AS
   SELECT l.id AS id,
         l.name || '@' || d.name AS address,
      l.name AS name,
      d.name AS domain,
      l.moderation_level AS moderation_level,
      l.moderation_regex AS moderation_regex,
      l.spamscore_threshold AS spamscore_threshold,
      l.maxsizemoderate AS maxsizemoderate,
      l.maxsizedrop AS maxsizedrop,
      a.name AS archiveserver,
      replace(a.urlpattern, '%', l.name) AS archiveaddress,
      replace(d.lists_helppage, '%', l.name) AS helppage,
      a.maildomain AS archivedomain,
      l.blacklist_regex AS blacklist_regex,
      l.whitelist_regex AS whitelist_regex,
      l.tagged_delivery AS tagged_delivery,
      l.tagkey AS tagkey,
      l.taglistsource AS taglistsource,
      l.send_moderation_notices AS send_moderation_notices
   FROM lists_list l
   INNER JOIN lists_domain d ON d.id=l.domain_id
   LEFT JOIN lists_archiveserver a ON a.id=l.archivedat_id
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglist_subscribers AS
   SELECT list_id AS listid,
         ls.subscriber_id AS subscriberaddress_id,
      email,
      eliminatecc,
      token,
      s.user_id AS userid,
      (SELECT array_agg(listtag_id) FROM lists_listsubscription_tags lst WHERE lst.listsubscription_id=ls.id) AS tags
   FROM lists_listsubscription ls
   INNER JOIN lists_subscriberaddress sa ON sa.id=ls.subscriber_id
   LEFT JOIN lists_subscriber s ON s.user_id=sa.subscriber_id
   WHERE confirmed AND subscription_confirmed AND NOT nomail
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglist_whitelist AS
   SELECT list_id AS listid,
         address AS email
   FROM lists_listwhitelist
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglist_globalwhitelist AS
   SELECT address AS email
   FROM lists_globalwhitelist
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW domains AS
   SELECT name AS domain
   FROM lists_domain
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglist_moderators AS
   SELECT llm.list_id AS listid,
             u.first_name || ' ' || u.last_name AS name,
      u.email
   FROM lists_list_moderators llm
   INNER JOIN lists_subscriber ls ON ls.user_id=llm.subscriber_id
   INNER JOIN auth_user u ON u.id=ls.user_id
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW subscriber_moderation_queue AS
   SELECT ls.subscriber_id,
          m.id as moderation_id,
      m.listid,
      m.sender,
      m.recipient,
      m.moderatedat,
      m.messageid
   FROM lists_subscriberaddress ls
   INNER JOIN moderation m ON m.sender=ls.email
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglist_globalblacklist AS
   SELECT address AS email
   FROM lists_globalblacklist
"""),


        migrations.RunSQL("""
CREATE OR REPLACE VIEW mailinglist_personalblacklist AS
    SELECT email
    FROM lists_subscriberaddress
    WHERE confirmed AND blacklisted
"""),


        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION verp_sender(listaddr text, subscriberaddress_id int, outgoing_id bigint)
RETURNS text
LANGUAGE sql
AS $$
   SELECT replace(listaddr, '@', '-owner+M' || subscriberaddress_id::text || '-' || outgoing_id::text || '@');
$$
"""),


        migrations.RunSQL("""
CREATE OR REPLACE FUNCTION recipient_list_headers(webroot text, listid int, token text)
RETURNS text
LANGUAGE sql
AS $$
   SELECT 'List-Unsubscribe: <' || webroot || '/unsub/' || listid::text || '/' || token || '/>' || chr(13) || chr(10);
$$"""),

    ]
