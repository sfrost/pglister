# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0004_order_whitelist'),
    ]

    operations = [
        migrations.AddField(
            model_name='list',
            name='shortdesc',
            field=models.TextField(blank=True),
        ),
    ]
