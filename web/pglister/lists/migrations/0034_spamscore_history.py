# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-12-05 19:49
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0033_cc_and_bcc_policies'),
    ]

    operations = [
        migrations.RunSQL("ALTER TABLE moderation ADD COLUMN spamscore NUMERIC(5,1) NULL"),
        migrations.RunSQL("CREATE TABLE mail_history_reasons (id int PRIMARY KEY, reason text NOT NULL)"),
        migrations.RunSQL("""WITH t (id, reason) AS (VALUES
        (0, 'Delivered directoy'),
        (1, 'Delivered from moderation'),
        (2, 'Discarded from moderation'),
        (3, 'Rejected from moderation'),
        (4, 'Dropped by blacklist'),
        (5, 'Dropped by BCC policy'),
        (6, 'Dropped by CC policy'),
        (7, 'Exceeded size limit')
) INSERT INTO mail_history_reasons (id, reason) SELECT id, reason FROM t ON CONFLICT (id) DO UPDATE SET reason=excluded.reason"""),

        migrations.RunSQL("""CREATE TABLE mail_history (
messageid text NOT NULL,
ts timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
spamscore NUMERIC(5,1) NULL,
status int NOT NULL)"""),
        migrations.RunSQL("CREATE INDEX mail_history_ts_idx ON mail_history(ts)"),
        migrations.RunSQL("CREATE INDEX mail_history_messageid_idx ON mail_history(messageid)"),

        migrations.RunSQL("""CREATE TABLE mail_history_content (
messageid text NOT NULL,
ts timestamptz NOT NULL DEFAULT CURRENT_TIMESTAMP,
contents bytea NOT NULL)"""),
        migrations.RunSQL("CREATE INDEX mail_history_content_ts_idx ON mail_history_content(ts)"),
        migrations.RunSQL("CREATE INDEX mail_history_content_messageid_idx ON mail_history_content(messageid)"),
    ]
