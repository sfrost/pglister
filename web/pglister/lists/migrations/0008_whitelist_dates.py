# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('lists', '0007_auto_20161116_1142'),
    ]

    operations = [
        migrations.AddField(
            model_name='listwhitelist',
            name='added_at',
            field=models.DateTimeField(default=datetime.datetime(2017, 5, 26, 20, 31, 12, 975239), auto_now_add=True, db_index=True),
            preserve_default=False,
        ),
    ]
