# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArchiveServer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('urlpattern', models.CharField(max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Domain',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('lists_helppage', models.URLField(max_length=100)),
                ('archiveservers', models.ManyToManyField(to='lists.ArchiveServer', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='GlobalWhitelist',
            fields=[
                ('address', models.EmailField(max_length=254, serialize=False, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='List',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('subscription_policy', models.IntegerField(choices=[(0, b'Anybody can subscribe'), (1, b'Requires moderator approval'), (2, b'Automatically managed')])),
                ('moderation_level', models.IntegerField(choices=[(0, b'No moderation'), (1, b'Direct subscribers can post'), (2, b'Global subscribers can post'), (3, b'All emails are moderated'), (4, b'All emails are double-moderated')])),
                ('spamscore_threshold', models.DecimalField(null=True, max_digits=5, decimal_places=1, blank=True)),
                ('archivedat', models.ForeignKey(blank=True, to='lists.ArchiveServer', null=True)),
                ('domain', models.ForeignKey(to='lists.Domain')),
            ],
        ),
        migrations.CreateModel(
            name='ListGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('groupname', models.CharField(max_length=100)),
                ('sortkey', models.IntegerField(default=10)),
            ],
            options={'ordering': ('sortkey',)},
        ),
        migrations.CreateModel(
            name='ListSubscription',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subscription_confirmed', models.BooleanField(default=True)),
                ('nomail', models.BooleanField(default=False)),
                ('list', models.ForeignKey(to='lists.List')),
            ],
        ),
        migrations.CreateModel(
            name='ListSubscriptionModerationToken',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tokensent', models.DateTimeField()),
                ('token', models.TextField(unique=True)),
                ('subscription', models.ForeignKey(to='lists.ListSubscription')),
            ],
        ),
        migrations.CreateModel(
            name='ListWhitelist',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address', models.EmailField(max_length=254)),
                ('list', models.ForeignKey(to='lists.List')),
            ],
        ),
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('user', models.OneToOneField(primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='SubscriberAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('email', models.EmailField(unique=True, max_length=254)),
                ('confirmed', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='SubscriberAddressToken',
            fields=[
                ('subscriberaddress', models.OneToOneField(primary_key=True, serialize=False, to='lists.SubscriberAddress')),
                ('tokensent', models.DateTimeField(auto_now_add=True)),
                ('token', models.TextField(unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='subscriberaddress',
            name='subscriber',
            field=models.ForeignKey(blank=True, to='lists.Subscriber', null=True),
        ),
        migrations.AddField(
            model_name='listsubscription',
            name='subscriber',
            field=models.ForeignKey(to='lists.SubscriberAddress'),
        ),
        migrations.AddField(
            model_name='list',
            name='group',
            field=models.ForeignKey(to='lists.ListGroup'),
        ),
        migrations.AddField(
            model_name='list',
            name='moderators',
            field=models.ManyToManyField(to='lists.Subscriber', blank=False, null=False),
        ),
        migrations.AddField(
            model_name='list',
            name='subscribers',
            field=models.ManyToManyField(to='lists.SubscriberAddress', through='lists.ListSubscription', blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='listwhitelist',
            unique_together=set([('list', 'address')]),
        ),
    ]
