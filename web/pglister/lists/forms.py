from django import forms
from django.forms import ValidationError
from django.forms.widgets import HiddenInput

from models import SubscriberAddress, Subscriber, List, ListTag, ListSubscription

from pglister.util import exec_to_list


class ConfirmForm(forms.Form):
    confirm = forms.BooleanField(required=True)

    def __init__(self, prompt=None, cannotcomplete=None, *args, **kwargs):
        super(ConfirmForm, self).__init__(*args, **kwargs)
        self.fields['confirm'].help_text = prompt
        self.cannotcomplete = cannotcomplete

    def clean(self):
        d = super(ConfirmForm, self).clean()
        if self.cannotcomplete:
            raise ValidationError(self.cannotcomplete)
        return d


class AddMailForm(forms.Form):
    email = forms.EmailField(label="Address")
    email2 = forms.EmailField(label="Confirm address")

    def clean_email(self):
        e = self.cleaned_data['email'].lower()
        # Is somebody already using this one?
        aa = SubscriberAddress.objects.filter(email=e)
        if aa:
            # A subscriber address exists. Is it in use?
            if len(aa) != 1:
                raise ValidationError("Internal consistency error, wrong count")
            a = aa[0]

            if a.subscriber:
                raise ValidationError("This email address is already in use")

            if hasattr(a, 'subscriberaddresstoken'):
                raise ValidationError("This email address is already being validated")

            # Ok, this is an address that was manually merged into the system
            # In that case, we allow it as if it was empty, and we'll deal with
            # the merging when the link is clicked.
            return e
        else:
            return e

    def clean_email2(self):
        return self.cleaned_data['email2'].lower()

    def clean(self):
        cleaned_data = super(AddMailForm, self).clean()
        if cleaned_data.get('email') and cleaned_data.get('email2') and cleaned_data.get('email') != cleaned_data.get('email2'):
            self._errors['email2'] = self.error_class(['Emails do not match'])
        return cleaned_data


class TagMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.description


class SubscribeForm(forms.Form):
    listid = forms.ChoiceField(label="List")
    addr = forms.ModelChoiceField(queryset=None, label="Address")
    tags = TagMultipleChoiceField(queryset=None, label="Tags", widget=forms.CheckboxSelectMultiple, required=False,
                                  help_text='Pick the tags you want to subscribe to. If you pick no tags, all emails will be delivered')
    confirm = forms.BooleanField(help_text="Please confirm that you want to subscribe to this list")

    showform = True

    def __init__(self, subscriber=None, canpicklist=False, *args, **kwargs):
        super(SubscribeForm, self).__init__(*args, **kwargs)
        self.fields['addr'].queryset = SubscriberAddress.objects.filter(subscriber=subscriber, confirmed=True)
        self.fields['addr'].empty_label = None
        if canpicklist:
            # List can be picked, so we are at the "outer" view
            self.fields['listid'].choices = list(self._get_all_choices(subscriber))
            del self.fields['tags']
        else:
            self.fields['listid'].choices = list(self._get_all_choices(subscriber, kwargs['data']['listid']))
            # Can't pick list, but can pick tags if any
            self.list = List.objects.get(pk=kwargs['data']['listid'])
            if self.list.tagged_delivery:
                self.fields['tags'].queryset = ListTag.objects.filter(list=self.list)
            else:
                del self.fields['tags']

        if not len(self.fields['listid'].choices):
            self.showform = False

    def _get_all_choices(self, subscriber, restrict_to_id=None):
        lastname = None
        lastgroup = None
        params = {
            'subid': subscriber.pk,
        }
        if restrict_to_id:
            params['listid'] = restrict_to_id
            qextra = 'AND l.id=%(listid)s'
        else:
            qextra = ''

        for groupname, listid, listname, shortdesc in exec_to_list("SELECT g.groupname, l.id AS listid, l.name AS listname, l.shortdesc as shortdesc FROM lists_list l INNER JOIN lists_listgroup g ON l.group_id=g.id WHERE l.subscription_policy IN (0,1) AND l.id NOT IN (SELECT list_id FROM lists_listsubscription ls INNER JOIN lists_subscriberaddress sa ON sa.id=ls.subscriber_id  WHERE sa.subscriber_id=%(subid)s) {0} ORDER BY g.sortkey, g.groupname, l.name".format(qextra), params):
            if lastname != groupname:
                if lastgroup:
                    yield (lastname, lastgroup)
                lastname = groupname
                lastgroup = []
            lastgroup.append((listid, u"{0} ({1})".format(listname, shortdesc)))
        if lastgroup:
            yield (lastname, lastgroup)


class EditSubForm(forms.ModelForm):
    tags = TagMultipleChoiceField(queryset=None, label="Tags", widget=forms.CheckboxSelectMultiple, required=False,
                                  help_text='Pick the tags you want to subscribe to. If you pick no tags, all emails will be delivered')

    class Meta:
        model = ListSubscription
        fields = ['nomail', 'tags', ]
        widgets = {
            'tags': forms.CheckboxSelectMultiple,
        }

    def __init__(self, *args, **kwargs):
        super(EditSubForm, self).__init__(*args, **kwargs)
        if self.instance.list.tagged_delivery:
            self.fields['tags'].queryset = ListTag.objects.filter(list=self.instance.list)
        else:
            del self.fields['tags']
        self.fields['nomail'].regular_field = True
        self.fields['nomail'].label = "Disable mail delivery"


class ManageSubscribersUnsubscribeForm(ConfirmForm):
    sid = forms.IntegerField()
    notify = forms.BooleanField(help_text="Notify user of unsubscription", required=False)

    def __init__(self, *args, **kwargs):
        super(ManageSubscribersUnsubscribeForm, self).__init__(*args, **kwargs)
        self.fields['sid'].widget = HiddenInput()


class GlobalSettingsForm(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['eliminatecc', ]
