from selectable.base import ModelLookup
from selectable.registry import registry
from selectable.decorators import staff_member_required

from models import Subscriber


@staff_member_required
class SubscriberLookup(ModelLookup):
        model = Subscriber
        search_fields = (
            'user__username__icontains',
            'user__first_name__icontains',
            'user__last_name__icontains',
        )
        filters = {'user__is_active': True, }

        def get_item_value(self, item):
                # Display for currently selected item
                return u"%s (%s)" % (item.user.username, item.user.get_full_name())

        def get_item_label(self, item):
                # Display for choice listings
                return u"%s (%s)" % (item.user.username, item.user.get_full_name())


registry.register(SubscriberLookup)
