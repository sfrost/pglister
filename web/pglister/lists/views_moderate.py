from django.shortcuts import get_object_or_404
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import FormView, TemplateView, View
from django.template.loader import get_template
from django.contrib import messages
from django.contrib.auth.models import User
from django.db import connection, transaction
from django.conf import settings

import datetime

from models import ListSubscriptionModerationToken, ListWhitelist, List, ListSubscription

from util import LoginRequired, send_template_mail, send_template_mail_to_moderators
from util import get_unsubscribe_instructions
from util import SimpleWebResponse
from pglister.listlog.util import log, Level
from pglister.util import exec_to_dict

from lib.lists import MailingList, SubscriptionPolicies

MIN_BOUNCE_PERCENTAGE = 10


def _format_from(fromaddr, sender):
    if fromaddr != sender:
        return u"{0} ({1})".format(fromaddr, sender)
    return fromaddr


def notify_unsubscribed_user(ls):
    # Notify the user that they have been unsubscribed
    if ls.subscriber and ls.subscriber.subscriber:
        fullname = unicode(ls.subscriber.subscriber)
    else:
        fullname = ""
    send_template_mail(
        'mail/unsubscription_notify_user.txt',
        ls.list.owner_address(),
        ls.list.owner_name(),
        ls.subscriber.email,
        fullname,
        "Unsubscribed from {0}".format(ls.list.name),
        {
            'email': ls.subscriber.email,
            'list': ls.list,
        },
    )


def _moderate_approve(modid, whitelist, user):
    curs = connection.cursor()
    curs.execute("SELECT previous_moderator, moderation_level, messageid, sender, fromaddr, l.id, l.name FROM moderation m INNER JOIN lists_list l ON l.id=m.listid WHERE m.id=%(id)s FOR UPDATE OF m", {
        'id': modid,
    })
    if curs.rowcount == 0:
        return (True, "Message with id {0} no longer in the moderation queue. Somebody else probably handled it, so ignoring.".format(modid))
    previous, level, messageid, sender, fromaddr, listid, listname = curs.fetchall()[0]
    if level == 4:
        # Double moderation requested, so check status
        if previous == user.id:
            return (False, "Message {0} was previously moderated by you. Awaiting second moderator.".format(messageid))
        if not previous:
            curs.execute("UPDATE moderation SET previous_moderator=%(user)s WHERE id=%(id)s", {
                'user': user.id,
                'id': modid,
            })
            log(Level.INFO, user, u"Approved post from {0} to {1}. Awaiting second moderator.".format(_format_from(fromaddr, sender), listname), messageid)
            return (True, "Message {0} flagged as approved. List is double moderated, so waiting for second moderator.".format(messageid))

        # Else this is a second moderation and the first one was OK, so
        # fall through to regular approval.
        log(Level.INFO, user, "Second moderator approval received.", messageid)
    curs.execute("UPDATE moderation SET approved='t' WHERE id=%(id)s", {
        'id': modid,
    })
    curs.execute("NOTIFY moderation")

    log(Level.INFO, user, u"Approved post from {0} to {1}. Message queued for delivery.".format(_format_from(fromaddr, sender), listname), messageid)

    if whitelist:
        # We always add to the local whitelist. The global whitelist is globally
        # managed. We might want to give it two different options in the future.
        ListWhitelist.objects.get_or_create(list_id=listid, address=fromaddr.lower())
        log(Level.INFO, user, u'Added email {0} to whitelist for {1}.'.format(fromaddr.lower(), listname), messageid)

        # If there are other emails in the mod queue for the same list
        # by the same email address, approve them as well if the user
        # was added to the whitelist.
        # We do this in all cases except when they need double approval.
        if level != 4:
            curs.execute("UPDATE moderation SET approved='t' WHERE approved='f' AND listid=%(listid)s AND sender=%(sender)s RETURNING messageid", {
                'listid': listid,
                'sender': fromaddr.lower(),
            })
            res = curs.fetchall()
            if res:
                for m, in res:
                    log(Level.INFO, user, u'Approved post from {0} to {1} due to addition to whitelist.'.format(_format_from(fromaddr, sender), listname), m)

                curs.execute("NOTIFY moderation")

                return (True, "Message {0} approved. {1} additional messages approved by whitelisting.".format(messageid, len(res)))

    return (True, "Message {0} approved.".format(messageid))


def _moderate_discard(modid, user):
    curs = connection.cursor()
    curs.execute("""WITH d AS (DELETE FROM moderation WHERE id=%(id)s RETURNING *),
i  AS (INSERT INTO mail_history_content (messageid, contents) SELECT messageid,contents FROM d),
i2 AS (INSERT INTO mail_history (messageid, spamscore, status) SELECT messageid, spamscore, 2 FROM d)
SELECT sender,fromaddr,messageid,l.name
FROM d INNER JOIN lists_list l ON l.id=d.listid""", {
        'id': modid,
    })
    if curs.rowcount == 0:
        return u"Message with id {0} no longer in the moderation queue. Somebody else probably handled it, so ignoring.".format(modid)
    if curs.rowcount != 1:
        raise Exception("Cannot happen, deleted more than one message!")
    sender, fromaddr, messageid, listname = curs.fetchall()[0]
    log(Level.INFO, user, u"Discarded post from {0} to {1}.".format(_format_from(fromaddr, sender), listname), messageid)
    return u"Discarded message {0}.".format(messageid)


def _moderate_unsubscribe(modid, user):
    curs = connection.cursor()

    sp = transaction.savepoint()
    curs.execute("WITH d AS (DELETE FROM moderation WHERE id=%(id)s RETURNING *) SELECT listid, sender,fromaddr,messageid,l.name FROM d INNER JOIN lists_list l ON l.id=d.listid", {
        'id': modid,
    })
    if curs.rowcount == 0:
        transaction.savepoint_rollback(sp)
        return u"Message with id {0} no longer in the moderation queue. Somebody else probably handled it, so ignoring.".format(modid)
    if curs.rowcount != 1:
        raise Exception("Cannot happen, deleted more than one message!")
    listid, sender, fromaddr, messageid, listname = curs.fetchall()[0]

    # Now try to unsubscribe the user as well
    try:
        l = List.objects.get(pk=listid)
        ls = ListSubscription.objects.get(list=l, subscriber__email=fromaddr)
    except ListSubscription.DoesNotExist:
        transaction.savepoint_rollback(sp)
        return u"Email {0} is not a subscriber to list with id {1}".format(fromaddr, listid)

    if l.subscription_policy == SubscriptionPolicies.MANAGED:
        transaction.savepoint_rollback(sp)
        return u"This list has it's membership automatically managed. Unsubscription cannot be completed."

    notify_unsubscribed_user(ls)

    # Actually unsubscribe
    ls.delete()

    log(Level.INFO, user, u"Discarded post from {0} to {1} and unsubscribed user.".format(fromaddr, l.name), messageid)

    transaction.savepoint_commit(sp)
    return u"Discarded message {0} and unsubscribed {1} from list {2}.".format(messageid, fromaddr, l.name)


def _moderate_reject(modid, request, reason, logreason):
    # Actually reject a post. This includes sending an email to the
    # poster letting them know why.
    curs = connection.cursor()
    curs.execute("""WITH d AS (DELETE FROM moderation WHERE id=%(id)s RETURNING *),
i  AS (INSERT INTO mail_history_content (messageid, contents) SELECT messageid,contents FROM d),
i2 AS (INSERT INTO mail_history (messageid, spamscore, status) SELECT messageid, spamscore, 3 FROM d)
SELECT sender,fromaddr,subject,messageid,l.id,l.name
FROM d INNER JOIN lists_list l ON l.id=d.listid""", {
        'id': modid,
    })
    if curs.rowcount == 0:
        messages.info(request, "Message with id {0} no longer in the moderation queue. Somebody else probably handled it, so ignoring.".format(modid))
        return
    if curs.rowcount != 1:
        raise Exception("Cannot happen, deleted more than one message!")
    sender, fromaddr, subject, messageid, listid, listname = curs.fetchall()[0]
    # Generate rejection email
    ml = MailingList.get_by_id(connection, listid)
    send_template_mail('mail/message_rejected.txt',
                       ml.owner_address(),
                       ml.owner_name(),
                       sender,
                       '',
                       u"Your message to {0}".format(listname),
                       {
                           'listname': listname,
                           'messageid': messageid,
                           'subject': subject,
                           'reason': reason,
                       })
    log(Level.INFO, request.user, u"Rejected post from {0} to {1} with reason '{2}'".format(_format_from(fromaddr, sender), listname, logreason), messageid)
    messages.info(request, u"Rejected message {0}.".format(messageid))


class Moderate(LoginRequired, TemplateView):
    template_name = 'moderate.html'

    def __init__(self, *args, **kwargs):
        super(Moderate, self).__init__(*args, **kwargs)

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.globalview = (self.request.GET.get('global', '0') == '1' or self.request.POST.get('global', '0') == '1')
        else:
            self.globalview = False
        return super(Moderate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Moderate, self).get_context_data(**kwargs)
        context['globalview'] = self.globalview

        if self.globalview:
            # View all moderation entries, not just the explicitly
            # listed ones. Mind that the columns have to be the same
            # ones as in the below views.
            context['modlist'] = exec_to_dict("""SELECT m.id, l.name AS listname, sender, fromaddr, moderatedat, approved, messageid, reason, reasonextra, subject, _to AS "to", cc, pg_size_pretty(length(contents)::bigint) as size, intro, previous_moderator, EXISTS (SELECT 1 FROM mailinglist_subscribers mls WHERE mls.email=m.fromaddr AND mls.listid=m.listid) AS is_subscribed, spamscore FROM moderation m INNER JOIN lists_list l ON l.id=m.listid ORDER BY approved, moderatedat LIMIT 100""")
            context['sublist'] = exec_to_dict("SELECT s.id, l.name AS listname, a.email, t.tokensent FROM lists_listsubscriptionmoderationtoken t INNER JOIN lists_listsubscription s ON s.id=t.subscription_id INNER JOIN lists_subscriberaddress a ON a.id=s.subscriber_id INNER JOIN lists_list l ON l.id=s.list_id WHERE NOT s.subscription_confirmed ORDER BY t.tokensent")
            context['bounces'] = exec_to_dict(
                """WITH bounced AS (
   SELECT list,subscriber,email,count(DISTINCT deliveryid) AS bounces FROM matched_bounces m INNER JOIN outgoing_completed c ON (c.id=m.deliveryid) INNER JOIN lists_subscriberaddress sa ON sa.id=m.subscriber INNER JOIN lists_listsubscription ls ON ls.list_id=list AND ls.subscriber_id=sa.id WHERE dt>now()-'1 month'::interval AND NOT transient GROUP BY list,subscriber,email)
, delivered AS (
  SELECT m.id,m.address,count(*) AS deliveries FROM outgoing_completed c INNER JOIN mailinglists m ON m.address=c.sendinglist WHERE completedts>now()-'1 month'::interval GROUP BY m.id, m.address
)
SELECT list,address,subscriber,email,bounces,deliveries,bounces*100/deliveries AS percentage
FROM bounced INNER JOIN delivered ON bounced.list=delivered.id
WHERE bounces*100/deliveries > %(threshold)s ORDER BY address,email""",
                {
                    'threshold': MIN_BOUNCE_PERCENTAGE,
                }
            )
            context['pendingdomains'] = exec_to_dict("SELECT split_part(lower(sender), '@', 2) AS addr, count(*) AS num FROM moderation GROUP BY addr HAVING count(*) > 1 ORDER BY 2 DESC")

            context['lists'] = List.objects.all()
        else:
            context['modlist'] = exec_to_dict(
                """SELECT m.id, l.name AS listname, sender, fromaddr, moderatedat, approved, messageid, reason, reasonextra, subject, _to AS "to", cc, pg_size_pretty(length(contents)::bigint) as size, intro, previous_moderator, EXISTS (SELECT 1 FROM mailinglist_subscribers mls WHERE mls.email=m.fromaddr AND mls.listid=m.listid) AS is_subscribed, spamscore FROM moderation m INNER JOIN lists_list l ON l.id=m.listid INNER JOIN lists_list_moderators lm ON lm.list_id=l.id INNER JOIN lists_subscriber s ON s.user_id=lm.subscriber_id WHERE s.user_id=%(userid)s ORDER BY approved, moderatedat LIMIT 100""",
                {
                    'userid': self.request.user.id,
                }
            )
            context['sublist'] = exec_to_dict(
                "SELECT s.id, l.name AS listname, a.email, t.tokensent FROM lists_listsubscriptionmoderationtoken t INNER JOIN lists_listsubscription s ON s.id=t.subscription_id INNER JOIN lists_subscriberaddress a ON a.id=s.subscriber_id INNER JOIN lists_list l ON l.id=s.list_id INNER JOIN lists_list_moderators lm ON lm.list_id=l.id INNER JOIN lists_subscriber mod ON mod.user_id=lm.subscriber_id WHERE NOT s.subscription_confirmed AND mod.user_id=%(userid)s ORDER BY t.tokensent",
                {
                    'userid': self.request.user.id,
                }
            )
            context['bounces'] = exec_to_dict(
                """WITH bounced AS (
  SELECT list,subscriber,email,count(*) AS bounces FROM matched_bounces m INNER JOIN outgoing_completed c ON (c.id=m.deliveryid) INNER JOIN lists_subscriberaddress sa ON sa.id=m.subscriber INNER JOIN lists_listsubscription ls ON ls.list_id=list AND ls.subscriber_id=sa.id INNER JOIN lists_list_moderators lm ON lm.list_id=m.list INNER JOIN lists_subscriber mod ON mod.user_id=lm.subscriber_id WHERE dt>now()-'1 month'::interval AND mod.user_id=%(userid)s AND NOT transient GROUP BY list,subscriber,email)
, delivered AS (
  SELECT m.id,m.address,count(*) AS deliveries FROM outgoing_completed c INNER JOIN mailinglists m ON m.address=c.sendinglist WHERE completedts>now()-'1 month'::interval GROUP BY m.id, m.address
)
SELECT list,address,subscriber,email,bounces,deliveries,bounces*100/deliveries AS percentage
FROM bounced INNER JOIN delivered ON bounced.list=delivered.id
WHERE bounces*100/deliveries > %(threshold)s ORDER BY address,email""",
                {
                    'userid': self.request.user.id,
                    'threshold': MIN_BOUNCE_PERCENTAGE,
                }
            )

            context['lists'] = List.objects.filter(moderators__user=self.request.user)

        if self.request.user.is_superuser:
            ival = datetime.timedelta(days=settings.BLACKLIST_UNUSED_WARNING_DAYS)
            context['blacklist_top'] = exec_to_dict("SELECT address, usecount, lastused FROM lists_globalblacklist ORDER BY usecount DESC LIMIT 10")
            context['blacklist_unused'] = exec_to_dict("SELECT address, usecount, lastused FROM lists_globalblacklist WHERE CURRENT_TIMESTAMP-lastused > %(interval)s OR lastused IS NULL ORDER BY lastused", {
                'interval': ival,
            })
            context['blacklist_unused_interval'] = ival

        return context

    def get_self_url(self):
        if self.globalview:
            return '/moderate/?global=1'
        return '/moderate/'

    @transaction.atomic
    def post(self, request):
        if request.POST.get('submod', '0') == '1':
            return self._moderate_subscribers(request)
        else:
            return self._moderate_messages(request)

    def _moderate_messages(self, request):
        curs = connection.cursor()
        for k, v in request.POST.items():
            if k.startswith('act_'):
                try:
                    n = int(k[4:])
                    v = int(v)
                except ValueError:
                    continue
                if v == 0:
                    # Ignore this one
                    continue
                elif v == 1 or v == 2:
                    # Approve, with or without whitelist

                    # We need to know if this list needs double moderation, so we need to
                    # do a read query to find that out. Since this is moderators only, we don't
                    # really care about it resulting in O(n) number of queries, as n will still
                    # be reasonably low...
                    (r, msg) = _moderate_approve(n, (v == 2), request.user)
                    if r:
                        messages.info(request, msg)
                    else:
                        messages.warning(request, msg)
                elif v == 3:
                    # Discard, that's easy enough
                    msg = _moderate_discard(n, request.user)
                    messages.info(request, msg)
                elif v == 4:
                    # reject
                    reason = request.POST.get('r_{0}'.format(n), "")
                    if not reason:
                        messages.warning(request, u"Cannot reject posts without specifying a reason!")
                        continue

                    _moderate_reject(n, request, reason, reason)
                elif v == 5:
                    # Unsubscribe, which implies discard
                    msg = _moderate_unsubscribe(n, request.user)
                    messages.info(request, msg)
                elif v in (6, 8):
                    # Blacklist, which implies discard of course.
                    # Superuser only!
                    if not self.request.user.is_superuser:
                        messages.error(self.request, "Blacklisting requires superuser access!")
                    else:
                        if v == 6:
                            curs.execute("INSERT INTO lists_globalblacklist (address, usecount, lastused) SELECT lower(sender), 1, CURRENT_TIMESTAMP FROM moderation WHERE id=%(id)s ON CONFLICT DO NOTHING RETURNING address", {'id': n})
                        else:
                            curs.execute("INSERT INTO lists_globalblacklist (address, usecount, lastused) SELECT '/@' || replace(lower(split_part(sender, '@', 2)), '.', '\\.') || '$', 1, CURRENT_TIMESTAMP FROM moderation WHERE id=%(id)s ON CONFLICT DO NOTHING RETURNING address", {'id': n})
                        if curs.rowcount:
                            blacklisted = curs.fetchone()[0]
                        else:
                            blacklisted = None

                        _moderate_discard(n, request.user)
                        if blacklisted:
                            messages.info(request, "{0} added to blacklist and message discarded.".format(blacklisted))
                            log(Level.INFO, self.request.user, u"Added {0} to global blacklist".format(blacklisted))

                            # If there are other messages from the same sender in the queue, discard those
                            if v == 6:
                                filt = "lower(sender)=%(sender)s"
                                senderparam = blacklisted
                            else:
                                filt = "lower(sender) ~ %(sender)s"
                                senderparam = blacklisted[1:]
                            curs.execute(
                                """WITH t AS (DELETE FROM moderation WHERE {0} RETURNING messageid, spamscore),
tt AS (INSERT INTO mail_history (messageid, spamscore, status)
 SELECT messageid, spamscore, 2 FROM t RETURNING messageid)
SELECT count(1) FROM tt""".format(filt),
                                {
                                    'sender': senderparam,
                                }
                            )
                            num = curs.fetchone()[0]
                            if num > 0:
                                messages.info(request, "{0} messages dropped from moderation queue due to blacklist".format(num))
                                curs.execute("UPDATE mailinglist_globalblacklist SET usecount=usecount+%(n)s, lastused=CURRENT_TIMESTAMP WHERE email=%(email)s", {
                                    'n': num,
                                    'email': blacklisted,
                                })
                                log(Level.INFO, self.request.user, u"Dropped {0} extra messages from moderation queue due to blacklisting of {1}.".format(num, blacklisted))

                        else:
                            messages.info(request, "Address already on blacklist, but message discarded")
                            # Already logged by _moderate_discard
                elif v == 7:
                    # Reject with pre-canned message about unsubscription
                    template = get_template('mail/unsubscribe_instructions.txt')
                    _moderate_reject(n, request, template.render({
                        'client_instructions': get_unsubscribe_instructions(),
                    }), 'unsubscribe instructions sent')
                else:
                    messages.error(self.request, "Invalid value for moderation: {0}".format(v))

        return HttpResponseRedirect(self.get_self_url())

    def _moderate_subscribers(self, request):
        for k, v in request.POST.items():
            if k.startswith('act_'):
                try:
                    n = int(k[4:])
                    v = int(v)
                except ValueError:
                    continue
                try:
                    s = ListSubscription.objects.get(pk=n)
                except ListSubscription.DoesNotExist:
                    messages.warning(request, "One or more moderation tokens no longer exist")
                    continue
                if v == 0:
                    # Ignore
                    continue
                elif v == 1:
                    messages.info(request,
                                  _perform_subscriber_approve(request, s))
                elif v == 2:
                    # Whitelist makes no sense here
                    raise Exception("Not implemented")
                elif v == 3:
                    # Discard
                    messages.info(request,
                                  _perform_subscriber_discard(request, s))
                elif v == 4:
                    # Reject
                    reason = request.POST.get('r_{0}'.format(n), "")
                    if not reason:
                        messages.warning(request, u"Cannot reject subscriptions without specifying a reason!")
                        continue
                    messages.info(request,
                                  _perform_subscriber_reject(request, s, reason))
                else:
                    messages.error(self.request, "Invalid value for moderation: {0}".format(v))

        return HttpResponseRedirect(self.get_self_url())


#####
# Moderate a message based on a link which was sent via email to the
# moderators.
#####
# Each moderator notice gets its own secret token, which is stored in
# moderator_notices.  That token is incorporated into URLs which go into the
# email sent to the moderator.  This is so moderators can just click on a link
# in their email to perform direct moderation.  Having the token be
# per-moderator allows us to identify which moderator performed the moderation.
class ModerateMail(FormView):
    def get(self, request, token, result):
        with transaction.atomic():
            curs = connection.cursor()
            curs.execute("""
            SELECT
                mod.id,
                llm.subscriber_id,
                mod.intro,
                mod._to, mod.cc
            FROM moderation mod
            JOIN moderator_notices mn ON (mod.id = mn.moderation_id)
            JOIN lists_list_moderators llm ON (mn.moderator_id = llm.id)
            WHERE mn.token=%(token)s
            """, {
                'token': token,
            })

            if curs.rowcount != 1:
                r = SimpleWebResponse(request, "Moderation", "Token does not exist.")
                r.status_code = 404
                r.reason_phrase = "Not found"
                return r

            (id, subscriber_id, intro, to, cc) = curs.fetchone()

            user = User.objects.get(id=subscriber_id)
            if result == 'approve':
                (r, msg) = _moderate_approve(id, False, user)
            elif result == 'whitelist':
                (r, msg) = _moderate_approve(id, True, user)
            elif result == 'discard':
                msg = _moderate_discard(id, user)
            elif result == 'preview':
                return HttpResponse(u"To: {0}\nCc: {1}\n\n{2}".format(to, cc, intro),
                                    content_type='text/plain')
            else:
                msg = 'Unsupported operation'

            return SimpleWebResponse(request, 'Moderation', msg)


#
# Below are the actual http views
#
class ModerateList(LoginRequired, FormView):

    @transaction.atomic
    def get(self, request, token, result):
        t = get_object_or_404(ListSubscriptionModerationToken, token=token)

        if result == 'reject':
            # For reject, we render the actual form
            raise Exception("Gimmearejectform")
        elif result == 'discard':
            msg = _perform_subscriber_discard(request, t.subscription)
        elif result == 'approve':
            msg = _perform_subscriber_approve(request, t.subscription)
        else:
            raise Exception("Can't happen")

        # If ?web=1 exists on the url then redirect back to the
        # moderation interface with a message. If not, it's clicked
        # in an email, so generate the smallest response possible.
        if request.GET.get('web', '0') == '1':
            messages.info(request, msg)
            return HttpResponseRedirect('/moderate/')
        else:
            return HttpResponse(msg, content_type='text/plain')


def _perform_subscriber_discard(request, s):
    # Silently drop a request, not generating any emails etc
    log(Level.INFO, request.user, u"Discarded request for subscription to {0} by {1}.".format(s.list, s.subscriber.email))

    s.listsubscriptionmoderationtoken.delete()
    s.delete()
    return u"List subscription for {0} to {1} discared.".format(s.subscriber.email, s.list)


def _perform_subscriber_reject(request, s, reason):
    log(Level.INFO, request.user, u"Approved rejected for subscription to {0} by {1} with reason {2}.".format(s.list, s.subscriber.email, reason))

    send_template_mail('mail/subscription_rejected.txt',
                       s.list.owner_address(),
                       s.list.owner_name(),
                       s.subscriber.email,
                       s.subscriber.subscriber.fullname,
                       u"Subscription to {0} rejected".format(s.list),
                       {
                           'list': s.list,
                           'reason': reason,
                           'webroot': settings.WEB_ROOT,
                       })

    s.listsubscriptionmoderationtoken.delete()
    s.delete()

    return u"List subscription for {0} to {1} rejected.".format(s.subscriber.email, s.list)


def _perform_subscriber_approve(request, s):
    log(Level.INFO, request.user, u"Approved request for subscription to {0} by {1}.".format(s.list, s.subscriber.email))

    s.subscription_confirmed = True
    s.listsubscriptionmoderationtoken.delete()
    s.save()

    send_template_mail('mail/subscription_complete.txt',
                       s.list.owner_address(),
                       s.list.owner_name(),
                       s.subscriber.email,
                       s.subscriber.subscriber.fullname,
                       u"Subscribed to {0}".format(s.list),
                       {
                           'list': s.list,
                           'webroot': settings.WEB_ROOT,
                       })

    if s.list.notify_subscriptions:
        send_template_mail_to_moderators(
            s.list,
            'mail/subscription_notify.txt',
            u'Subscription to {0}'.format(s.list),
            {
                'email': s.subscriber.email,
            },
        )

    return u"Approved subscription to {0} by {1}.".format(s.list, s.subscriber.email)


class ModerateScript(LoginRequired, TemplateView):
    content_type = 'text/javascript'

    def get_template_names(self):
        return ['js/%s.js' % self.kwargs['j']]


class BlacklistDomain(LoginRequired, View):
    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise PermissionDenied()

        return super(BlacklistDomain, self).dispatch(*args, **kwargs)

    def post(self, request):
        domains = request.POST.getlist('domains')
        if not domains:
            messages.warning(request, "No domains selected, so none were blacklisted")
            return HttpResponseRedirect("../?global=1")

        with transaction.atomic():
            with connection.cursor() as curs:
                for d in domains:
                    re = '/@{0}$'.format(d.lower().replace('.', '\\.'))
                    curs.execute("INSERT INTO lists_globalblacklist (address, usecount, lastused) VALUES (%(addr)s, 1, CURRENT_TIMESTAMP) ON CONFLICT DO NOTHING RETURNING address", {
                        'addr': re,
                    })
                    if curs.rowcount:
                        # Find current matches and discard them
                        curs.execute(
                            """WITH t AS (DELETE FROM moderation WHERE lower(sender) ~ %(addr)s RETURNING messageid, spamscore),
tt AS (INSERT INTO mail_history (messageid, spamscore, status)
 SELECT messageid, spamscore, 2 FROM t RETURNING messageid)
SELECT count(1) FROM tt""",
                            {
                                'addr': re[1:],
                            }
                        )
                        num = curs.fetchall()[0][0]
                        messages.info(request, "{0} added to blacklist, {1} emails dropped as a result".format(d.lower(), num))
                        log(Level.INFO, request.user, "{0} added to blacklist, {1} emails dropped as a result".format(d.lower(), num))
                        curs.execute("UPDATE lists_globalblacklist SET usecount=%(num)s WHERE address=%(addr)s", {
                            'num': num,
                            'addr': re,
                        })
                    else:
                        messages.info(request, "{0} already on blacklist.".format(d.lower()))

        return HttpResponseRedirect("../?global=1")
