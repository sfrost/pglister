from django.contrib import admin
from django import forms

from selectable.forms.widgets import AutoCompleteSelectMultipleWidget
from lookups import SubscriberLookup

from models import Domain, List, ListGroup, ListAlias, ArchiveServer
from models import ListWhitelist, GlobalWhitelist, GlobalBlacklist
from models import MessageidModerationList
from models import GlobalModerationList


class SelectableWidgetAdminFormMixin(object):
        def __init__(self, *args, **kwargs):
                super(SelectableWidgetAdminFormMixin, self).__init__(*args, **kwargs)
                for fn in self.Meta.widgets.keys():
                        self.fields[fn].widget.can_add_related = False
                        self.fields[fn].widget.can_change_related = False
                        self.fields[fn].widget.can_delete_related = False


class ListWhitelistAdmin(admin.ModelAdmin):
    list_display = ('list', 'address', 'added_at')


class ListAdminForm(SelectableWidgetAdminFormMixin, forms.ModelForm):
    class Meta:
        model = List
        widgets = {
            'moderators': AutoCompleteSelectMultipleWidget(lookup_class=SubscriberLookup),
        }
        exclude = []

    def clean(self):
        data = super(ListAdminForm, self).clean()

        if data['tagged_delivery']:
            if not data['tagkey']:
                self.add_error('tagkey', 'Must be enabled when tagged delivery is enabled')
            if not data['taglistsource']:
                self.add_error('taglistsource', 'Must be enabled when tagged delivery is enabled')
        else:
            if data['tagkey']:
                self.add_error('tagkey', "Can't be enabled unless tagged delivery is enabled")
            if data['taglistsource']:
                self.add_error('taglistsource', "Can't be enabled unless tagged delivery is enabled")
        return data


class ListAliasForm(forms.ModelForm):
    class Meta:
        model = ListAlias
        fields = ['alias', ]

    def clean_alias(self):
        # List aliases can't be duplicated, but this could be a value used
        # by another *list*...
        (local, domain) = self.cleaned_data['alias'].lower().split('@')

        if List.objects.filter(name=local, domain__name=domain).exists():
            raise forms.ValidationError("A list with address {0} already exists".format(self.cleaned_data['alias'].lower()))

        return self.cleaned_data['alias'].lower()


class ListAliasInline(admin.TabularInline):
    model = ListAlias
    extra = 2
    form = ListAliasForm


class ListAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'domain', 'subscription_policy', 'moderation_level', 'archivedat')
    form = ListAdminForm
    inlines = [ListAliasInline, ]


admin.site.register(Domain)
admin.site.register(List, ListAdmin)
admin.site.register(ListGroup)
admin.site.register(ArchiveServer)
admin.site.register(ListWhitelist, ListWhitelistAdmin)
admin.site.register(GlobalWhitelist)
admin.site.register(GlobalBlacklist)
admin.site.register(MessageidModerationList)
admin.site.register(GlobalModerationList)
