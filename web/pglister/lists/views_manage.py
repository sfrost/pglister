from django.shortcuts import get_object_or_404, Http404
from django.http import HttpResponseRedirect
from django.views.generic import FormView, TemplateView, ListView, RedirectView
from django.contrib import messages
from django.db import connection, transaction
from django.core.validators import validate_email
from django.conf import settings

import requests

from lib.misc import generate_random_token

from util import LoginRequired, send_template_mail_to_moderators
from util import SimpleWebResponse
from pglister.listlog.util import log, Level
from pglister.util import exec_to_dict

from models import List, ListWhitelist, SubscriberAddress, ListSubscription
from models import MessageidModerationList
from forms import ManageSubscribersUnsubscribeForm

from views_moderate import notify_unsubscribed_user


class ManageList(LoginRequired, TemplateView):
    template_name = 'managelist.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List, id=kwargs['id'])
        else:
            self.list = get_object_or_404(List, id=kwargs['id'], moderators__user=self.request.user)
        return super(ManageList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageList, self).get_context_data(**kwargs)

        context['list'] = self.list
        context['whitelist'] = ListWhitelist.objects.filter(list=self.list)

        return context

    @transaction.atomic
    def post(self, request, id):
        if request.POST['op'] == 'sub':
            return self._post_subscribe(request)
        if request.POST['op'] == 'wl':
            return self._post_whitelist(request)
        else:
            raise Exception("Not implemented")

    def _post_subscribe(self, request):
        m = request.POST['subscriberemail'].lower()
        try:
            validate_email(m)
        except:
            messages.error(request, u'Email address {0} has invalid format.'.format(m))
            return HttpResponseRedirect('.')

        # If a subscriber exists, pick it up. If not, create a new one and set if to confirmed
        # so it can immediately start receiving emails. It will not be connected to an account,
        # but that will be done whenever the user connects.
        s, created = SubscriberAddress.objects.get_or_create(email=m, defaults={'confirmed': True, 'token': generate_random_token()})
        s.save()

        # We bypass moderation as this request by definition already came from a moderator
        ls, created = ListSubscription.objects.get_or_create(list=self.list, subscriber=s)
        if created:
            log(Level.INFO, request.user, u"Subscribed address {0} to {1}.".format(m, self.list))
            messages.info(request, u"Subscribed address {0} to {1}.".format(m, self.list))

            if self.list.notify_subscriptions:
                send_template_mail_to_moderators(self.list,
                                                 'mail/subscription_notify.txt',
                                                 u'Subscription to {0}'.format(self.list),
                                                 {'email': m},
                                                 )
        else:
            messages.warning(request, u"Address {0} already subscribed.".format(m))
        return HttpResponseRedirect('.')

    def _post_whitelist(self, request):
        if request.POST.get('addemail', None):
            m = request.POST['addemail'].lower()
            try:
                validate_email(m)
            except:
                messages.error(request, u'Email address {0} has invalid format.'.format(m))
                return HttpResponseRedirect('.')
            (lw, created) = ListWhitelist.objects.get_or_create(list=self.list, address=m)
            if created:
                messages.info(request, u"Added {0} to whitelist".format(m))
                log(Level.INFO, request.user, u"Added {0} to whitelist for {1}".format(m, self.list))
            else:
                messages.warning(request, u"{0} already on whitelist".format(m))

        for k in request.POST.getlist('delwl'):
            wl = ListWhitelist.objects.get(pk=k, list=self.list)
            messages.info(request, u"Deleted {0} from whitelist".format(wl.address))
            log(Level.INFO, request.user, "Deleted {0} from whitelist for {1}".format(wl.address, self.list))
            wl.delete()
        return HttpResponseRedirect('.')


class ManageSubscribers(LoginRequired, ListView):
    model = ListSubscription
    paginate_by = 100
    template_name = 'managelist_subscribers.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List, id=kwargs['id'])
        else:
            self.list = get_object_or_404(List, id=kwargs['id'], moderators__user=self.request.user)
        return super(ManageSubscribers, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageSubscribers, self).get_context_data(**kwargs)

        if 'search' in self.request.GET:
            context['baseurl'] = u'?search={0}&'.format(self.request.GET['search'])
        else:
            context['baseurl'] = '?'
        return context

    def get_queryset(self):
        if self.request.GET.get('search', None):
            return super(ManageSubscribers, self).get_queryset().filter(list=self.list, subscriber__email__icontains=self.request.GET['search']).order_by('subscriber__email')
        else:
            return super(ManageSubscribers, self).get_queryset().filter(list=self.list).order_by('subscriber__email')


class ManageGlobalSubscribers(LoginRequired, ListView):
    model = ListSubscription
    paginate_by = 100
    template_name = 'managelist_global_subscribers.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise Http404("Nope")
        return super(ManageGlobalSubscribers, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageGlobalSubscribers, self).get_context_data(**kwargs)

        context['search'] = self.request.GET['search']
        if 'search' in self.request.GET:
            context['baseurl'] = u'?search={0}&'.format(self.request.GET['search'])
        else:
            context['baseurl'] = '?'
        return context

    def get_queryset(self):
        if self.request.GET.get('search', None):
            return super(ManageGlobalSubscribers, self).get_queryset().filter(subscriber__email__icontains=self.request.GET['search']).order_by('subscriber__email')
        else:
            # No hits!
            return []

    @transaction.atomic
    def post(self, request):
        ids = [int(k.split('_')[1]) for k, v in request.POST.items() if k.startswith('s_') and v == '1']
        if not ids:
            messages.error(request, "No addresses selected for unsubscription!")
            return HttpResponseRedirect("?search={0}".format(request.POST['search']))

        notify = request.POST.get('notify', '') == '1'

        for id in ids:
            ls = get_object_or_404(ListSubscription, id=id)
            if notify:
                notify_unsubscribed_user(ls)
            log(Level.INFO, request.user, u"Unsubscribed user {0} from {1}.".format(ls.subscriber.email, ls.list))
            ls.delete()
        messages.info(request, "Unsubscribed {0} users.".format(len(ids)))
        return HttpResponseRedirect("../../../?global=1")


class ManageThreadBlacklist(LoginRequired, ListView):
    model = MessageidModerationList
    paginate_by = 100
    template_name = 'managelist_thread_blacklist.html'

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_superuser:
            raise Http404("Nope")
        return super(ManageThreadBlacklist, self).dispatch(*args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ManageThreadBlacklist, self).get_context_data(*args, **kwargs)
        context['has_archivesapi'] = len(settings.ARCHIVES_API_SERVERS) > 0
        return context

    def archives_messageid_list(self):
        for s in settings.ARCHIVES_API_SERVERS:
            try:
                r = requests.get(s, timeout=5)
                if r.status_code == 200:
                    for m in r.json():
                        yield m
            except:
                pass

    @transaction.atomic
    def post(self, request):
        if request.POST['submit'] == "Remove":
            id = int(request.POST['delid'])
            item = get_object_or_404(MessageidModerationList, pk=id)
            m = item.messageid
            log(Level.INFO, request.user, u"Removed {0} from messageid blacklist".format(m), m)
            item.delete()
            messages.info(request, "{0} removed from blacklist.".format(m))
            return HttpResponseRedirect(".")
        elif request.POST['submit'] == "Add to blacklist":
            messageid = request.POST['messageid'].lstrip('<').rstrip('>')
            if MessageidModerationList.objects.filter(messageid=messageid).exists():
                messages.info(request, "{0} is already on the blacklist.".format(messageid))
            else:
                MessageidModerationList(messageid=messageid).save()
                log(Level.INFO, request.user, u"Added {0} to messageid blacklist".format(messageid), messageid)
                messages.info(request, "{0} added to blacklist.".format(messageid))
            return HttpResponseRedirect(".")
        raise Exception("Unknown submit")


class ManageSubscribersUnsubscribe(LoginRequired, FormView):
    template_name = 'baseform.html'
    success_url = '../..'
    form_class = ManageSubscribersUnsubscribeForm
    savebutton = 'Unsubscribe'
    cancelurl = '../'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List, id=kwargs['id'])
        else:
            self.list = get_object_or_404(List, id=kwargs['id'], moderators__user=self.request.user)
        return super(ManageSubscribersUnsubscribe, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        s = ListSubscription.objects.get(pk=self.request.POST['sid'])
        if s.list != self.list:
            raise Exception("Internal consistency error")

        kw = super(ManageSubscribersUnsubscribe, self).get_form_kwargs()
        kw['prompt'] = u'Please confirm that you want to unsubscribe {0} from {1}.'.format(s.subscriber, self.list)
        return kw

    @transaction.atomic
    def form_valid(self, form):
        s = ListSubscription.objects.get(pk=form.cleaned_data['sid'])

        if form.cleaned_data['notify']:
            notify_unsubscribed_user(s)
        log(Level.INFO, self.request.user, u"Unsubscribed {0} from {1}".format(s.subscriber, s.list))
        messages.info(self.request, u"Unsubscribed {0} from {1}".format(s.subscriber, s.list))
        s.delete()

        return super(ManageSubscribersUnsubscribe, self).form_valid(form)


class ManageBounces(LoginRequired, TemplateView):
    template_name = 'bounces.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_superuser:
            self.list = get_object_or_404(List, id=kwargs['listid'])
        else:
            self.list = get_object_or_404(List, id=kwargs['listid'], moderators__user=self.request.user)

        self.sub = get_object_or_404(SubscriberAddress, pk=kwargs['sub'], listsubscription__list=self.list)

        return super(ManageBounces, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ManageBounces, self).get_context_data(**kwargs)
        context['list'] = self.list
        context['sub'] = self.sub
        context['bounces'] = exec_to_dict("""WITH t AS (SELECT DISTINCT ON (deliveryid) id, deliveryid, dt, transient, header, message FROM matched_bounces WHERE list=%(list)s AND subscriber=%(sub)s AND dt>now()-'1 month'::interval ORDER BY deliveryid, dt DESC)
SELECT t.id,t.deliveryid,t.dt, c.messageid, c.completedts, t.transient, string_to_array(t.header, chr(10)) AS header, string_to_array(t.message, chr(10)) AS message FROM t
RIGHT JOIN outgoing_completed c ON c.id=t.deliveryid
WHERE c.completedts>now()-'1 month'::interval
 AND c.sendinglist=(SELECT address FROM mailinglists WHERE id=%(list)s) ORDER BY completedts DESC""", {
            'list': self.list.pk,
            'sub': self.sub.pk,
        })
        if self.request.user.is_superuser:
            # Find bounce status for this user on other lists
            context['otherlists'] = exec_to_dict(
                """WITH bounced AS (
   SELECT list,count(DISTINCT deliveryid) AS bounces FROM matched_bounces m INNER JOIN outgoing_completed c ON (c.id=m.deliveryid) WHERE m.subscriber=%(subid)s AND dt>now()-'1 month'::interval AND NOT transient GROUP BY list)
, delivered AS (
  SELECT m.id,m.address,count(*) AS deliveries FROM outgoing_completed c INNER JOIN mailinglists m ON m.address=c.sendinglist WHERE completedts>now()-'1 month'::interval GROUP BY m.id, m.address
)
SELECT list AS listid,name,bounces,deliveries,bounces*100/deliveries AS percentage
FROM bounced INNER JOIN delivered ON bounced.list=delivered.id
INNER JOIN lists_list l ON l.id=bounced.list
ORDER BY list""",
                {
                    'subid': self.sub.pk,
                }
            )
            context['all_listids'] = [l['listid'] for l in context['otherlists']]
        return context

    @transaction.atomic
    def post(self, request, listid, sub):
        if 'transientid' in request.POST:
            curs = connection.cursor()
            curs.execute("UPDATE matched_bounces SET transient=NOT transient WHERE id=%(bounceid)s AND subscriber=%(subid)s", {
                'bounceid': int(request.POST['transientid']),
                'subid': self.sub.id,
            })
            return HttpResponseRedirect(".")
        elif 'unsub' in request.POST:
            if request.POST.get('unsub') == '1':
                s = get_object_or_404(ListSubscription, list=self.list, subscriber=self.sub)

                log(Level.INFO, self.request.user, u"Unsubscribed {0} from {1} from bounce".format(s.subscriber, s.list))
                messages.info(self.request, u"Unsubscribed {0} from {1}".format(s.subscriber, s.list))
                s.delete()
            elif request.POST.get('unsub') == '2':
                # Unsubscribe from *all* specified lists
                if not request.user.is_superuser:
                    raise Http404("Nope")

                for s in ListSubscription.objects.filter(subscriber=self.sub, list__in=request.POST.get('listidlist').split(',')):
                    log(Level.INFO, self.request.user, u"Unsubscribed {0} from {1} from bounce".format(s.subscriber, s.list))
                    messages.info(self.request, u"Unsubscribed {0} from {1}".format(s.subscriber, s.list))
                    s.delete()

            return HttpResponseRedirect("../../")
        else:
            return SimpleWebResponse(request, "Invalid form data")


class DeleteBlacklistEntries(LoginRequired, RedirectView):
    permanent = False
    url = '/moderate/'

    def post(self, request):
        addrs = [k for k, v in request.POST.items() if v == "1" and k not in ('csrfmiddlewaretoken', )]
        if len(addrs) == 0:
            messages.info(self.request, "No rules selected, nothing deleted")
            return super(DeleteBlacklistEntries, self).post(request)

        # Else find the matching rules and, well, delete them
        curs = connection.cursor()
        curs.execute(
            """WITH t AS (DELETE FROM lists_globalblacklist WHERE address=ANY(%(addrs)s) RETURNING address),
    t2 AS (INSERT INTO log (level, source, msg, username) SELECT 0, 'web', 'Removed global blacklist entry for ''' || address || '''', %(user)s FROM t)
SELECT address FROM t""",
            {
                'addrs': addrs,
                'user': request.user.username,
            },
        )
        for a, in curs.fetchall():
            messages.warning(request, "Deleted blacklist entry {}".format(a))

        return super(DeleteBlacklistEntries, self).post(request)
