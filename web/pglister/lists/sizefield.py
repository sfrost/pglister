from django.core.validators import EMPTY_VALUES
from django.core.exceptions import ValidationError
from django import forms
from django.db import models

from lib.misc import format_size


def parse_size(size):
    if isinstance(size, int):
        return size

    if size.endswith('B'):
        if size.endswith('KB'):
            return int(size[:-2]) * 1024
        elif size.endswith('MB'):
            return int(size[:-2]) * (1024 * 1024)
        elif size.endswith('GB'):
            return int(size[:-2]) * (1024 * 1024 * 1024)
        raise ValueError("Unknown suffix")
    else:
        raise ValueError("Don't know how to parse this")


class SizeField(models.BigIntegerField):
    def formfield(self, **kwargs):
        kwargs['widget'] = SizeWidget
        return super(SizeField, self).formfield(**kwargs)

    def to_python(self, value):
        if value is None:
            return None
        try:
            return parse_size(value)
        except ValueError:
            raise ValidationError('Invalid value')


class SizeWidget(forms.TextInput):
    def render(self, name, value, attrs=None):
        if value:
            try:
                value = format_size(value)
            except ValueError:
                pass
        return super(SizeWidget, self).render(name, value, attrs)

    def value_from_datadict(self, data, files, name):
        value = super(SizeWidget, self).value_from_datadict(data, files, name)
        if value not in EMPTY_VALUES:
            try:
                return parse_size(value)
            except ValueError:
                pass
        return value
