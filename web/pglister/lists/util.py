from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db import connection
from django.template.loader import get_template
from django.conf import settings

import os

from lib.mailutil.simple import make_simple_mail


class LoginRequired(object):
    @classmethod
    def as_view(cls):
        return login_required(super(LoginRequired, cls).as_view())


def SimpleWebResponse(request, title, txt):
    return render(request, 'simple.html', {
        'title': title,
        'txt': txt,
    })


def send_template_mail(templatename, senderaddr, sendername, recipientaddr, recipientname, subject, contextdict={}, headers={}):
    template = get_template(templatename)
    msg = make_simple_mail(senderaddr,
                           sendername,
                           recipientaddr,
                           recipientname,
                           subject,
                           template.render(contextdict),
                           headers=headers,
                           )
    curs = connection.cursor()
    curs.execute("INSERT INTO admin_out(sender, recipient, contents) values (%(sender)s, %(recipient)s, %(contents)s)", {
        'sender': settings.CONFIRM_SENDER_ADDRESS,
        'recipient': recipientaddr,
        'contents': msg.as_string(),
    })
    curs.execute("NOTIFY admin_out")


def send_template_mail_to_moderators(l, templatename, subject, contextdict):
    contextdict.update({
        'list': l,
    })
    for m in l.moderators.all():
        send_template_mail(
            templatename,
            l.moderator_notice_address(),
            l.moderator_notice_name(),
            m.user.email,
            m.fullname,
            subject,
            contextdict,
        )


def get_unsubscribe_instructions():
    root = os.path.join(os.path.dirname(__file__), 'templates/unsubscribe_instructions/')
    for fn in sorted(os.listdir(root)):
        if not fn.endswith('.txt'):
            continue
        with open(os.path.join(root, fn)) as f:
            yield (fn[:-4], f.read().replace("\n", ' '))
