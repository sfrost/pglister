self.addEventListener('activate', function(e) {
  console.log('[ServiceWorker] Activate');
});

self.addEventListener('fetch', function(event) {
//  console.log('[ServiceWorker] fetch');
  event.respondWith(fetch(event.request));
});
