from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe

from lib.lists import ModerationReason

register = template.Library()


@register.filter(is_safe=True)
def label_class(value, arg):
        return value.label_tag(attrs={'class': arg})


@register.filter(is_safe=True)
def field_class(value, arg):
        return value.as_widget(attrs={"class": arg})


@register.filter(name='alertmap')
@stringfilter
def alertmap(value):
        if value == 'error':
                return 'alert-danger'
        elif value == 'warning':
                return 'alert-warning'
        elif value == 'success':
                return 'alert-success'
        else:
                return 'alert-info'


@register.filter(is_safe=True)
def ischeckbox(obj):
    return obj.field.widget.__class__.__name__ == "CheckboxInput" and not getattr(obj.field, 'regular_field', False)


@register.filter(is_safe=True)
def isrequired_error(obj):
    if obj.errors and obj.errors[0] == u"This field is required.":
        return True
    return False


@register.filter(is_safe=True)
def splitlines(obj):
    return obj.split("\n")


@register.filter
def moderation_reason(obj, extra):
    return ModerationReason(obj, extra).full_string()


@register.simple_tag(takes_context=True)
def moderation_options(context, reason, is_subscribed):
    return mark_safe("\n".join(('<option value="{0}">{1}</option>'.format(k, v) for k, v in ModerationReason(int(reason)).get_moderation_options(is_subscribed, context['user'].is_superuser))))
