from datetime import datetime

from models import LogEntry, Level

__all__ = [
    'log', 'Level',
]


def log(level, user, msg, messageid=None):
    if level < 0:
        level = 0
    if level > 2:
        level = 2
    # As we're on a different database connection this will always run
    # in it's own transaction.
    LogEntry(t=datetime.now(),
             level=level,
             source='web',
             username=user and user.username or '',
             messageid=messageid,
             msg=msg).save()
