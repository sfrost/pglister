from django.contrib import admin

from models import LogEntry
from models import levelstrings


class LevelFilter(admin.SimpleListFilter):
    title = 'Level'
    parameter_name = 'level'

    def lookups(self, request, model_admin):
        return zip(range(len(levelstrings)), levelstrings)

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(level=self.value())
        return queryset


class LogEntryAdmin(admin.ModelAdmin):
    model = LogEntry
    list_display = ('t', 'levelstr', 'confirmed', 'source', 'username', 'messageid', 'msg')
    search_fields = ('username', 'messageid', 'msg')
    list_filter = (LevelFilter, 'confirmed', 'source')
    actions = ('flag_as_confirmed', )

    def levelstr(self, o):
        return o.levelstr
    levelstr.short_description = 'Level'

    def flag_as_confirmed(self, request, queryset):
        queryset.update(confirmed=True)
    flag_as_confirmed.short_description = 'Flag selected items as confirmed'


admin.site.register(LogEntry, LogEntryAdmin)
